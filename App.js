import React from 'react'
import { ActivityIndicator, View } from 'react-native';
import AppNavigator from './src/components/AppNavigator'
import { Font } from 'expo'

export default class App extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            fontLoaded: false
        }
    }
    
    async componentDidMount(){
        await Font.loadAsync({
            'zcool': require('./assets/fonts/ZCOOLQingKeHuangYou-Regular.ttf'),
            'roboto-regular': require('./assets/fonts/Roboto-Regular.ttf'),
            'roboto-bold': require('./assets/fonts/Roboto-Bold.ttf')
        }).then(() => this.setState({ fontLoaded: true }))
    }

    render(){
        return (
            this.state.fontLoaded ?
            <AppNavigator/> :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large'/>
            </View>
        )
    }

}

import { Alert, AsyncStorage } from 'react-native'
import axios from 'axios'
import { StackActions, NavigationActions } from 'react-navigation'

const server = 'http://www.appdoprefeito.com.br:3000'
// const server = 'http://192.168.1.15:3000'

const strangers = [
    { code: '00', char: 'ç' },
    { code: '01', char: 'Ç' },
    { code: '02', char: 'á' },
    { code: '03', char: 'Á' },
    { code: '04', char: 'é' },
    { code: '05', char: 'É' },
    { code: '06', char: 'í' },
    { code: '07', char: 'Í' },
    { code: '08', char: 'ó' },
    { code: '09', char: 'Ó' },
    { code: '10', char: 'ú' },
    { code: '11', char: 'Ú' },
    { code: '12', char: 'à' },
    { code: '13', char: 'À' },
    { code: '14', char: 'â' },
    { code: '15', char: 'Â' },
    { code: '16', char: 'ê' },
    { code: '17', char: 'Ê' },
    { code: '18', char: 'ô' },
    { code: '19', char: 'Ô' },
    { code: '20', char: 'ã' },
    { code: '21', char: 'Ã' },
    { code: '22', char: 'õ' },
    { code: '23', char: 'Õ' },
    { code: '24', char: 'º' },
    { code: '25', char: 'ª' }
]

const logout = async (navigation) => {

    await axios.post(`${server}/user/insert`, {
        id: navigation.getParam('id'),
        token: ''
    })
        .then()
        .then(() => {
            delete axios.defaults.headers.common['Authorization']
            AsyncStorage.removeItem('userData')
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Loading' })]
            })
            navigation.dispatch(resetAction)
        })
        .catch(_ => Alert.alert('Aviso', 'O aplicativo não conseguiu sair do sistema de forma bem-sucedida. Por favor, entre em contato com o administrador.'))
}

//Pega os dados da API
const getPayDayMessage = async (id, thisReference) => {

    const url = `user/payment/${id}`
    await axios.get(`${server}/${url}`)
    .then((response) => thisReference.setState({ payDayMessage: response.data }),
        (_) => thisReference.setState({ payDayMessage: undefined })
    )
    //.then(_ => this.setState({ loading: false }))
    .catch(_ => Alert.alert('Erro ao buscar dados', 'Tente novamente mais tarde ou entre em contato com o administrador.'))
}

export { server, strangers, logout, getPayDayMessage }
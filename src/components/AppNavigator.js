import React from 'react'
import { createStackNavigator, createAppContainer } from 'react-navigation'
import StackViewStyleInterpolator from 
  'react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator'
import { View, Text, StyleSheet, Dimensions, TouchableWithoutFeedback } from 'react-native'
import { FontAwesome, Feather } from '@expo/vector-icons'

//Import das telas para navegação
import AuthOrApp from '../screens/AuthOrApp'
import Login from '../screens/Login'
import PasswordRecovery from '../screens/PasswordRecovery'
import ChooseMenu from '../screens/ChooseMenu'
import EditUser from '../screens/EditUser'
import Financial from '../screens/Financial'
import Education from '../screens/Education'
import Health from '../screens/Health'
import Personal from '../screens/Personal'
import Fleet from '../screens/Fleet'
import News from '../screens/News'
import History from '../screens/History'
import Charts from '../screens/Charts'
import Cauc from '../screens/Cauc'
import MiniBrowser from '../screens/MiniBrowser'

const styles = StyleSheet.create({
    titleArea: {
        height: '100%', 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    title: {
        color: '#E5C80D', 
        fontFamily: 'zcool',
        fontSize: Dimensions.get('window').width * 0.07
    },
    button: {
        height: '60%', 
        width: '90%', 
        borderWidth: 1, 
        borderColor: '#FFF',
        alignItems: 'center', 
        justifyContent: 'center', 
        paddingHorizontal: 10,
        borderRadius: 7
    }
})

const renderTitle = (title) =>
    <View style={styles.titleArea}>
        <Text style={styles.title}>
            {title}
        </Text>
    </View>

const renderMenuButton = (toggleMenu) =>
    <TouchableWithoutFeedback onPress={toggleMenu}>
        <View style={styles.button}>
            <FontAwesome name="navicon" color="white"
                size={Dimensions.get('window').width * 0.06}/>
        </View>
    </TouchableWithoutFeedback>

const renderBackButton = (navigation) => 
    <TouchableWithoutFeedback onPress={() => navigation.navigate('ChooseMenu', 
        { title: 'APP DO PREFEITO', 
            id_city: navigation.getParam('id_city'),
            id: navigation.getParam('id'),
            city: navigation.getParam('city'),
            email: navigation.getParam('email'),
            name: navigation.getParam('name'),
            payDayMessage: navigation.getParam('payDayMessage') })}>
        <View style={{ paddingHorizontal: 8 }}>
            <Feather name="arrow-left" color="white" size={Dimensions.get('window').width * 0.06}/>
        </View>
    </TouchableWithoutFeedback>

const AppNavigator = createStackNavigator({
    Loading: { screen: AuthOrApp },
    Login: { screen: Login },
    PasswordRecovery: { screen: PasswordRecovery },
    ChooseMenu: { screen: ChooseMenu },
    EditUser: { screen: EditUser },
    Financial: { screen: Financial },
    Education: { screen: Education },
    Health: { screen: Health },
    Personal: { screen: Personal },
    Fleet: { screen: Fleet },
    News: { screen: News },
    History: { screen: History },
    Charts: { screen: Charts },
    Cauc: { screen: Cauc },
    MiniBrowser: { screen: MiniBrowser }
}, {
    initialRouteName: "Loading",
    transitionConfig: () => { //Transição das telas na horizontal
        return {screenInterpolator: StackViewStyleInterpolator.forHorizontal}
    },
    defaultNavigationOptions: ({navigation}) => ({
        headerStyle: {
            backgroundColor: '#0E4B8D'
        },
        headerTintColor: '#FFF',
        headerTitle: renderTitle(navigation.getParam('title', 'APP DO PREFEITO')),
        headerRight: renderMenuButton(navigation.getParam('toggleMenu')),
        headerTitleContainerStyle: {
            justifyContent: 'center'
        },
        headerLeft: renderBackButton(navigation),
        gesturesEnabled: false
    })
})

const App = createAppContainer(AppNavigator)

export default App
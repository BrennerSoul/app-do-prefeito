import React from 'react'
import { View, StyleSheet, Dimensions, Text, ImageBackground } from 'react-native'
import Button from './Button'
import { LinearGradient } from 'expo'

export default class Body extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            greeting: undefined
        }
    }

    render() {

        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/img/background_menu.jpg')}
                resizeMode='stretch'>
                <LinearGradient style={styles.container}
                    colors={['rgba(255,255,255,0.3)', 'rgba(102,125,182,0.3)']}>
                    <View style={styles.messageBox}>
                        <Text style={styles.messageLabel}>BEM-VINDO,</Text>
                        <Text style={styles.messageLabel}>
                            SR(A). {this.props.navigation.getParam('name').toUpperCase()}!
                        </Text>
                        {this.props.payDayMessage !== 'OK' && this.props.payDayMessage !== '' ?
                            <Text style={styles.warningLabel}>
                                {this.props.payDayMessage}
                            </Text> :
                            <Text style={[styles.messageLabel, { padding: 10 }]}>
                                ESCOLHA UMA DAS OPÇÕES ABAIXO:
                            </Text>
                        }
                    </View>
                    <View style={styles.column}>
                        <View style={styles.line}>
                            <Button label='RECEITAS' imgPath={require('../../assets/img/finances.png')}
                                onClick={() => this.props.navigation.navigate('Financial', {
                                    title: 'RECEITAS',
                                    id_city: this.props.navigation.getParam('id_city'),
                                    id: this.props.navigation.getParam('id'),
                                    city: this.props.navigation.getParam('city'),
                                    email: this.props.navigation.getParam('email'),
                                    name: this.props.navigation.getParam('name')
                                })} />
                            <Button label='SAÚDE' imgPath={require('../../assets/img/health.png')}
                                onClick={() => this.props.navigation.navigate('Health', {
                                    title: 'SAÚDE',
                                    id_city: this.props.navigation.getParam('id_city'),
                                    id: this.props.navigation.getParam('id'),
                                    city: this.props.navigation.getParam('city'),
                                    email: this.props.navigation.getParam('email'),
                                    name: this.props.navigation.getParam('name')
                                })} />
                            <Button label='PESSOAL' imgPath={require('../../assets/img/personal.png')}
                                onClick={() => this.props.navigation.navigate('Personal', {
                                    title: 'PESSOAL',
                                    id_city: this.props.navigation.getParam('id_city'),
                                    id: this.props.navigation.getParam('id'),
                                    city: this.props.navigation.getParam('city'),
                                    email: this.props.navigation.getParam('email'),
                                    name: this.props.navigation.getParam('name')
                                })} />
                        </View>
                        <View style={styles.line}>
                            <Button label='EDUCAÇÃO' imgPath={require('../../assets/img/education.png')}
                                onClick={() => this.props.navigation.navigate('Education', {
                                    title: 'EDUCAÇÃO',
                                    id_city: this.props.navigation.getParam('id_city'),
                                    id: this.props.navigation.getParam('id'),
                                    city: this.props.navigation.getParam('city'),
                                    email: this.props.navigation.getParam('email'),
                                    name: this.props.navigation.getParam('name')
                                })} />
                            <Button label='FROTA' imgPath={require('../../assets/img/fleet.png')}
                                onClick={() => this.props.navigation.navigate('Fleet', {
                                    title: 'FROTA',
                                    id_city: this.props.navigation.getParam('id_city'),
                                    id: this.props.navigation.getParam('id'),
                                    city: this.props.navigation.getParam('city'),
                                    email: this.props.navigation.getParam('email'),
                                    name: this.props.navigation.getParam('name')
                                })} />
                            <Button label='NOTÍCIAS' imgPath={require('../../assets/img/news.png')}
                                onClick={() => this.props.navigation.navigate('News', {
                                    title: 'NOTÍCIAS',
                                    id_city: this.props.navigation.getParam('id_city'),
                                    id: this.props.navigation.getParam('id'),
                                    city: this.props.navigation.getParam('city'),
                                    email: this.props.navigation.getParam('email'),
                                    name: this.props.navigation.getParam('name')
                                })} />
                        </View>
                        <View style={styles.line}>
                            <Button label='HISTÓRICO' imgPath={require('../../assets/img/history.png')}
                                onClick={() => this.props.navigation.navigate('History', {
                                    title: 'HISTÓRICO',
                                    id_city: this.props.navigation.getParam('id_city'),
                                    id: this.props.navigation.getParam('id'),
                                    city: this.props.navigation.getParam('city'),
                                    email: this.props.navigation.getParam('email'),
                                    name: this.props.navigation.getParam('name')
                                })} />
                            <Button label='GRÁFICOS' imgPath={require('../../assets/img/charts.png')}
                                onClick={() => this.props.navigation.navigate('Charts', {
                                    title: 'GRÁFICOS',
                                    id_city: this.props.navigation.getParam('id_city'),
                                    id: this.props.navigation.getParam('id'),
                                    city: this.props.navigation.getParam('city'),
                                    email: this.props.navigation.getParam('email'),
                                    name: this.props.navigation.getParam('name')
                                })} />
                            <Button label='CAUC' imgPath={require('../../assets/img/cauc.png')}
                                onClick={() => this.props.navigation.navigate('Cauc', {
                                    title: 'CAUC',
                                    id_city: this.props.navigation.getParam('id_city'),
                                    id: this.props.navigation.getParam('id'),
                                    city: this.props.navigation.getParam('city'),
                                    email: this.props.navigation.getParam('email'),
                                    name: this.props.navigation.getParam('name')
                                })} />
                        </View>
                    </View>
                </LinearGradient>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center', //Coluna
        justifyContent: 'center', //Linha
        height: '100%',
        width: '100%'
    },
    messageBox: {
        flex: 2, //20% height
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    messageLabel: {
        color: '#000',
        fontSize: Dimensions.get('window').width * 0.045,
        fontFamily: 'roboto-bold',
        textAlign: 'center'
    },
    warningLabel: {
        color: 'red',
        fontSize: Dimensions.get('window').width * 0.045,
        fontFamily: 'roboto-bold',
        textAlign: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: '#F5A9A9',
        borderWidth: 0.5,
        borderColor: 'red'
    },
    column: {
        flex: 8, //80% height
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    line: {
        width: '100%',
        height: '25%',
        flexDirection: 'row',
        justifyContent: 'space-around'
    }
})
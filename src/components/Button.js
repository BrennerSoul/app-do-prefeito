import React from 'react'
import { View, TouchableOpacity, Image, StyleSheet, Dimensions, Text } from 'react-native'

export default props => 
    <View style={{width: '30%', height: '100%'}}>
        <TouchableOpacity style={styles.button} onPress={props.onClick}>
            <View style={styles.subButton}>
                <Image resizeMode='stretch' style={styles.image} source={props.imgPath}/>
                <Text style={styles.labelButton}>{props.label}</Text>
            </View>
        </TouchableOpacity>
    </View>

const styles = StyleSheet.create({
    button: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        backgroundColor: '#0E4B8D',
        borderColor: '#0E1254',
        borderRadius: 10
    },
    subButton: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
        padding: 10
    },
    image: {
        width: '90%',
        height: '70%'
    },
    labelButton: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.035,
        color: '#FFF',
        textAlign: 'center'
    }
})
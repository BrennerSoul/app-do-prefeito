import React from 'react'
import { View, Text, TouchableWithoutFeedback, StyleSheet } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'

export default class CheckBox extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            checked: false
        }
    }

    render(){
        return(
            <View style={[styles.container, this.props.style]}>
                <TouchableWithoutFeedback onPress={() => this.setState({ checked: !this.state.checked })}>
                    <View style={styles.check}>
                        {this.state.checked ? 
                        <FontAwesome name="check" size={20} color="black"/>
                        : null}
                    </View>
                </TouchableWithoutFeedback>
                <Text style={styles.label}>Decrescente</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    check: {
        borderColor: '#000',
        borderWidth: 1,
        backgroundColor: '#FFF',
        width: 20,
        height: 20
    }
})
import React from 'react'
import { Modal, Text, View, StyleSheet, TouchableWithoutFeedback, Linking, TouchableOpacity, 
    Dimensions } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'

export default props =>
    <Modal onRequestClose={props.onCancel} visible={props.isVisible} 
        animationType='slide' transparent={true}>
        <TouchableWithoutFeedback onPress={props.onCancel}>
            <View style={styles.offset}/>
        </TouchableWithoutFeedback>
        <View style={styles.modal}>
            <View style={styles.firstLine}>
                <Text style={styles.title}>DESENVOLVIDO POR:</Text>
                <TouchableWithoutFeedback onPress={props.onCancel}>
                    <FontAwesome name='close' color='gray' size={Dimensions.get('window').width * 0.06}/>
                </TouchableWithoutFeedback>
            </View>
            <View style={styles.developersArea}>
                <View style={styles.dev1}>
                    <Text style={styles.name}>BRENNER DE ARAÚJO RODRIGUES BARBOSA</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <FontAwesome name='phone-square' color='black' 
                            size={Dimensions.get('window').width * 0.07}/>
                        <Text style={styles.label}>+55 34 99159-7448</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <FontAwesome name='envelope-square' color='black' 
                            size={Dimensions.get('window').width * 0.07}/>
                        <Text style={styles.label}>brennerdaraujo@gmail.com</Text>
                    </View>
                    <TouchableOpacity onPress={() => Linking.openURL(
                        'https://www.linkedin.com/in/brenner-de-araújo-rodrigues-barbosa-b96a086b'
                    )}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <FontAwesome name='linkedin-square' color='black' 
                                size={Dimensions.get('window').width * 0.07}/>
                            <Text style={[styles.label, { textDecorationLine: 'underline', 
                                color: '#01A9DB' }]}>
                                LinkedIn
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.dev1}>
                    <Text style={styles.name}>FREDERICO DA SILVA</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <FontAwesome name='phone-square' color='black' 
                            size={Dimensions.get('window').width * 0.07}/>
                        <Text style={styles.label}>+55 34 98438-1917</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <FontAwesome name='envelope-square' color='black' 
                            size={Dimensions.get('window').width * 0.07}/>
                        <Text style={styles.label}>fred.rocke02@gmail.com</Text>
                    </View>
                    <TouchableOpacity onPress={() => Linking.openURL(
                        'https://www.linkedin.com/in/03fred'
                    )}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <FontAwesome name='linkedin-square' color='black' 
                                size={Dimensions.get('window').width * 0.07}/>
                            <Text style={[styles.label, { textDecorationLine: 'underline', 
                                color: '#01A9DB' }]}>
                                LinkedIn
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
        <TouchableWithoutFeedback onPress={props.onCancel}>
            <View style={styles.offset}/>
        </TouchableWithoutFeedback>
    </Modal>

const styles = StyleSheet.create({
    offset: {
        width: '100%',
        height: '20%',
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    modal: {
        width: '100%',
        height: '60%',
        backgroundColor: 'white'
    },
    firstLine: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1
    },
    title: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.06,
        color: '#000'
    },
    developersArea: {
        flex: 1
    },
    dev1: {
        height: '50%',
        width: '100%',
        justifyContent: 'space-around',
        paddingLeft: 10,
        borderBottomWidth: 1,
        borderTopWidth: 1
    },
    name: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.045,
        color: '#000'
    },
    label: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.045,
        color: '#000',
        paddingLeft: 8
    }
})
import React from 'react'
import { View, Text, StyleSheet, Dimensions, StatusBar, TouchableHighlight } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation'

class Header extends React.Component {

    render() {
        return (
            <View style={ styles.container }>
                <View style={styles.logo}>
                    <Text style={ styles.title }>APP DO PREFEITO</Text>
                </View>
                <TouchableHighlight underlayColor={'orange'} style={styles.button} 
                    onPress={() => this.props.navigation.navigate('Login')}>
                    <View style={styles.buttonView}>
                        <FontAwesome name="sign-out" 
                            size={(Dimensions.get('window').height - StatusBar.currentHeight) 
                            * 0.1 * 0.6 * 0.7 /* 70% do botão */} 
                            color="white" />
                        <Text style={ styles.exit }>Sair</Text>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}

export default withNavigation(Header)

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: '10%',
        width: '100%',
        backgroundColor: '#1E1D1E',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        flex: 7.5, //width 75%,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        color: '#E5C80D',
        fontFamily: 'zcool',
        fontSize: (Dimensions.get('window').height - StatusBar.currentHeight) * 0.1 * 0.5 //50% do logo
    },
    button: {
        flex: 2.5, //width 25%
        height: '60%',
        marginRight: 10,
        justifyContent: 'center',
        backgroundColor: 'red',
        borderRadius: 10
    },
    buttonView: {
        height: '100%',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: 8
    },
    exit: {
        fontFamily: 'roboto-bold',
        color: '#FFF',
        //40% do botão
        fontSize: (Dimensions.get('window').height - StatusBar.currentHeight) * 0.1 * 0.6 * 0.4
    }
})
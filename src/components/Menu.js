//--------------------------------------Imports--------------------------------------------
import React, { Component } from 'react'
import {
    Animated, View, Text, StyleSheet, Dimensions, TouchableWithoutFeedback, Alert,
    TouchableOpacity, AsyncStorage
} from 'react-native'
import axios from 'axios'
import { StackActions, NavigationActions } from 'react-navigation'
import { MaterialIcons } from '@expo/vector-icons'
import { server } from '../common'

//--------------------------------------menuObject--------------------------------------------
const menuLinks = [
    { id: 1, label: 'RECEITAS', navValue: 'Financial', iconName: 'monetization-on' },
    { id: 2, label: 'SAÚDE', navValue: 'Health', iconName: 'local-hospital' },
    { id: 3, label: 'PESSOAL', navValue: 'Personal', iconName: 'person' },
    { id: 4, label: 'EDUCAÇÃO', navValue: 'Education', iconName: 'school' },
    { id: 5, label: 'FROTA', navValue: 'Fleet', iconName: 'directions-bus' },
    { id: 6, label: 'NOTÍCIAS', navValue: 'News', iconName: 'library-books' },
    { id: 7, label: 'HISTÓRICO', navValue: 'History', iconName: 'storage' },
    { id: 8, label: 'GRÁFICOS', navValue: 'Charts', iconName: 'insert-chart' },
    { id: 9, label: 'CAUC', navValue: 'Cauc', iconName: 'assignment-late' },
    { id: 10, label: 'EDITAR USUÁRIO', navValue: 'EditUser', iconName: 'edit' },
    { id: 11, label: 'SAIR', navValue: 'Login', iconName: 'exit-to-app' }
]

export default class Menu extends Component {

    //--------------------------------------State--------------------------------------------
    state = {
        sideBarX: new Animated.Value(Dimensions.get('window').width),
        isOpenedMenu: false
    }

    //--------------------------------------Lifecycles--------------------------------------------
    //Atribui a função toggleMenu ao clique do botão de menu
    componentDidMount() {
        this.props.navigation.setParams({ toggleMenu: this.toggleMenu });
    }

    componentDidUpdate() {
        if (this.state.isOpenedMenu) this.openModal()
        else this.closeModal()
    }

    render() {
        return (
            <Animated.View zIndex={1} position='absolute'
                style={[styles.sideBar, { transform: [{ translateX: this.state.sideBarX }] }]}>
                <TouchableWithoutFeedback onPress={() => { this.setState({ isOpenedMenu: false }) }}>
                    <View style={styles.offset} />
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Text style={styles.title}>MENU</Text>
                    </View>
                    <View style={styles.body}>
                        {this.renderMenuItems(this.props.excludeItem)}
                    </View>
                </View>
            </Animated.View>
        )
    }

    //-----------------------------------Methods---------------------------------------
    toggleMenu = () => {
        this.setState({ isOpenedMenu: !this.state.isOpenedMenu })
    }
    
    openModal = () => {
        Animated.timing(this.state.sideBarX, {
            duration: 300,
            toValue: 0
        }).start();
    }

    closeModal = () => {
        Animated.timing(this.state.sideBarX, {
            duration: 300,
            toValue: Dimensions.get('window').width
        }).start();
    }

    onPress = async (element) => {
        this.setState({ isOpenedMenu: false })

        if (element.id !== 11) {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({
                    type: 'Navigate',
                    routeName: element.navValue,
                    params: {
                        title: element.label,
                        id_city: this.props.navigation.getParam('id_city'),
                        id: this.props.navigation.getParam('id'),
                        city: this.props.navigation.getParam('city'),
                        email: this.props.navigation.getParam('email'),
                        name: this.props.navigation.getParam('name')
                    }
                })]
            })
            this.props.navigation.dispatch(resetAction)
        }
        else {

            await axios.post(`${server}/user/insert`, {
                id: this.props.navigation.getParam('id'),
                token: ''
            })
                .then()
                .then(() => {
                    delete axios.defaults.headers.common['Authorization']
                    AsyncStorage.removeItem('userData')
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'Loading' })]
                    })
                    this.props.navigation.dispatch(resetAction)
                })
                .catch(_ => Alert.alert('Aviso', 'O aplicativo não conseguiu sair do sistema de forma bem-sucedida. Por favor, entre em contato com o administrador.'))

        }
    }

    renderMenuItems = (excludeItem) => {
        const items = []

        menuLinks.forEach((element) => {
            if (element.label != excludeItem)
                items.push(
                    <TouchableOpacity key={element.id} style={{ width: '100%', height: '9%' }}
                        onPress={() => this.onPress(element)}>
                        <View style={styles.button}>
                            <MaterialIcons name={element.iconName} color='white'
                                size={Dimensions.get('window').width * 0.07} />
                            <Text style={styles.menuLink}>{element.label}</Text>
                        </View>
                    </TouchableOpacity>
                )
        })

        return items
    }
}

//-----------------------------------Styles---------------------------------------
const styles = StyleSheet.create({
    sideBar: {
        flex: 1,
        height: '100%',
        flexDirection: 'row'
    },
    container: {
        width: '50%',
        height: '100%',
        backgroundColor: '#424242',
        borderWidth: 1,
        borderColor: '#000',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    offset: {
        width: '50%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0)'
    },
    header: {
        width: '100%',
        height: '10%',
        backgroundColor: '#FFF',
        justifyContent: 'center',
        borderTopLeftRadius: 5
    },
    title: {
        textAlign: 'center',
        color: '#000',
        fontFamily: 'zcool',
        fontSize: Dimensions.get('window').width * 0.08
    },
    body: {
        width: '100%',
        flex: 1
    },
    button: {
        flexDirection: 'row',
        width: '100%',
        height: '100%',
        borderBottomWidth: 1,
        borderColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    menuLink: {
        color: '#FFF',
        fontFamily: 'zcool',
        fontSize: Dimensions.get('window').width * 0.05
    }
})
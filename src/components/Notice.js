import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Dimensions } from 'react-native'

const goToNotice = (navigation, link) => {
    navigation.navigate('MiniBrowser', {title: 'NAVEGADOR', url: link})
}

export default props =>
    <View style={styles.container}>
        <View style={styles.noticeArea}>
            <Text style={styles.title}>{props.resumo.split('\t').join('')}</Text>
            <TouchableOpacity onPress={() => goToNotice(props.navigation, props.link.replace('\n', ''))}>
                <Text style={styles.linkLabel}>
                    Clique aqui para ir à notícia
                </Text>
            </TouchableOpacity>
            <Text style={styles.date}>Data de acesso: {props.data}</Text>
        </View>
    </View>

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1
    },
    noticeArea: {
        width: '100%',
        padding: 10
    },
    title: {
        fontFamily: 'roboto-bold',
        color: '#000',
        fontSize: Dimensions.get('window').width * 0.055,
        textAlign: 'justify'
    },
    linkLabel: {
        fontFamily: 'roboto-regular',
        color: '#0E4B8D',
        fontSize: Dimensions.get('window').width * 0.035,
        textDecorationLine: 'underline'
    },
    date: {
        fontFamily: 'roboto-regular',
        color: '#000',
        fontSize: Dimensions.get('window').width * 0.025,
        textAlign: 'right'
    }
})
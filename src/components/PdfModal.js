import React from 'react'
import {
    Modal, Text, View, StyleSheet, TouchableWithoutFeedback, BackHandler, ActivityIndicator, WebView, Alert, Dimensions
} from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import axios from 'axios'
import { getPayDayMessage, logout, server } from '../common'

export default class PdfModal extends React.Component {

    //--------------------------------------Constructor--------------------------------------------
    constructor(props) {
        super(props)
        this.state = {
            data: undefined,
            loading: true,
            payDayMessage: undefined,
            isOpenedPdf: false
        }
        this.mounted = false
    }

    //--------------------------------------API--------------------------------------------    
    async getData() {

        this.setState({ loading: true })
        await axios.get(`${server}/pdf/${this.props.navigation.getParam('id')}`)
            .then((response) => {
                this.setState({ data: response.data })
            },
                (_) => this.setState({ data: '404' })
            )
            .then(() => this.setState({ loading: false }))
            .catch(_ => Alert.alert('Erro ao buscar dados', 'Tente novamente mais tarde ou entre em contato com o administrador.'))
    }

    //--------------------------------------Lifecycles--------------------------------------------
    //Atribui a função openPdf ao clique do botão de menu
    async componentDidMount() {
        this.props.navigation.setParams({ openPdfModal: this.openPdf })
        this.mounted = true
        await getPayDayMessage(this.props.navigation.getParam('id'), this)

        //Botao Back do Android
        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ChooseMenu', {
                title: 'APP DO PREFEITO',
                id_city: this.props.navigation.getParam('id_city'),
                id: this.props.navigation.getParam('id'),
                city: this.props.navigation.getParam('city'),
                email: this.props.navigation.getParam('email'),
                name: this.props.navigation.getParam('name')
            })
            return true
        })
    }

    //Remove a função
    componentWillUnmount() {
        this.mounted = false
        if (this.backHandler)
            this.backHandler.remove()
    }

    async componentDidUpdate(_, prevState) {
        if (prevState.payDayMessage !== this.state.payDayMessage) {
            await getPayDayMessage(this.props.navigation.getParam('id'), this)

            if (this.state.payDayMessage === '' && this.mounted)
                await logout(this.props.navigation)
        }

        if (this.state.isOpenedPdf && prevState.isOpenedPdf !== this.state.isOpenedPdf)
            await this.getData()
    }

    render() {

        return (
            <Modal onRequestClose={() => this.setState({ isOpenedPdf: false })} visible={this.state.isOpenedPdf} animationType='slide' transparent={true}>
                <View style={{ height: '10%', backgroundColor: 'rgba(0,0,0,0)' }} />
                <View style={styles.modal}>
                    <View style={styles.firstLine}>
                        <Text style={styles.title}>Leitor de PDF</Text>
                        <TouchableWithoutFeedback onPress={() => this.setState({ isOpenedPdf: false })}>
                            <FontAwesome name='close' color='gray' size={Dimensions.get('window').width * 0.06} />
                        </TouchableWithoutFeedback>
                    </View>
                    <View style={styles.pdfArea}>
                        {this.state.loading ?
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator size='large' color='black' />
                            </View> :
                            this.state.data === '404' ?
                                <View style={styles.errorArea}>
                                    <Text style={styles.errorLabel}>PDF não encontrado</Text>
                                </View> :
                                <WebView
                                    source={{ uri: 'http://www.appdoprefeito.com.br:3000/pdf/1' }}
                                    originWhitelist={['*']}
                                    domStorageEnabled={true}
                                    // scalesPageToFit
                                    bounces={false}
                                    style={{ flex: 1 }}
                                    javaScriptEnabled={true}
                                />}
                    </View>
                </View>
            </Modal>
        )
    }

    //-----------------------------------Methods---------------------------------------
    openPdf = () => {
        this.setState({ isOpenedPdf: true })
    }
}

const styles = StyleSheet.create({
    modal: {
        width: '100%',
        height: '90%',
        backgroundColor: 'white'
    },
    firstLine: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1
    },
    title: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.06,
        color: '#000'
    },
    pdfArea: {
        flex: 1
    },
    errorArea: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorLabel: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.08,
        color: '#DF0101'
    }
})
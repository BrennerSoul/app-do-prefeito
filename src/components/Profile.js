//--------------------------------------Imports--------------------------------------------
import React from 'react'
import {
    Animated, View, Text, StyleSheet, Dimensions, TouchableWithoutFeedback, Alert,
    TouchableOpacity, Image, AsyncStorage
} from 'react-native'
import axios from 'axios'
import { StackActions, NavigationActions } from 'react-navigation'
import { FontAwesome } from '@expo/vector-icons'
import { server } from '../common'

export default class Profile extends React.Component {

    //--------------------------------------State--------------------------------------------
    state = {
        sideBarX: new Animated.Value(Dimensions.get('window').width),
        isOpenedMenu: false
    }

    //--------------------------------------Lifecycles--------------------------------------------
    //Atribui a função toggleMenu ao clique do botão de menu
    componentDidMount() {
        this.props.navigation.setParams({ toggleProfileMenu: this.toggleMenu });
    }

    componentDidUpdate() {
        if (this.state.isOpenedMenu) this.openModal()
        else this.closeModal()
    }

    render() {
        return (
            <Animated.View zIndex={1} position='absolute'
                style={[styles.sideBar, { transform: [{ translateX: this.state.sideBarX }] }]}>
                <TouchableWithoutFeedback onPress={() => { this.setState({ isOpenedMenu: false }) }}>
                    <View style={styles.offset} />
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Text style={styles.title}>PERFIL</Text>
                    </View>
                    <View style={styles.body}>
                        <View style={styles.backgroundImg}>
                            <Image style={styles.profileImg}
                                source={require('../../assets/img/mayor.png')}
                                resizeMode='stretch' borderRadius={20} />
                        </View>
                        <Text style={styles.name}>
                            {this.props.navigation.getParam('name')}
                        </Text>
                        <Text style={styles.subData}>
                            {this.props.navigation.getParam('email')}
                        </Text>
                        <Text style={styles.subData}>
                            {this.props.navigation.getParam('city').toUpperCase()}
                        </Text>
                        <TouchableOpacity onPress={this.editUser}>
                            <View style={[styles.button, { backgroundColor: '#07A20C' }]}>
                                <FontAwesome name="edit" color="white"
                                    size={Dimensions.get('window').width * 0.06} />
                                <Text style={styles.buttonLabel}>ALTERAR DADOS</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.logout}>
                            <View style={[styles.button, { backgroundColor: '#EC1515' }]}>
                                <FontAwesome name="sign-out" color="white"
                                    size={Dimensions.get('window').width * 0.06} />
                                <Text style={styles.buttonLabel}>SAIR</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </Animated.View>
        )
    }

    //-----------------------------------Methods---------------------------------------
    toggleMenu = () => {
        this.setState({ isOpenedMenu: !this.state.isOpenedMenu })
    }

    openModal = () => {
        Animated.timing(this.state.sideBarX, {
            duration: 300,
            toValue: 0
        }).start();
    }

    closeModal = () => {
        Animated.timing(this.state.sideBarX, {
            duration: 300,
            toValue: Dimensions.get('window').width
        }).start();
    }

    logout = async () => {

        await axios.post(`${server}/user/insert`, {
            id: this.props.navigation.getParam('id'),
            token: ''
        })
            .then()
            .then(() => {
                delete axios.defaults.headers.common['Authorization']
                AsyncStorage.removeItem('userData')
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Loading' })]
                })
                this.props.navigation.dispatch(resetAction)
            })
            .catch(_ => Alert.alert('Aviso', 'O aplicativo não conseguiu sair do sistema de forma bem-sucedida. Por favor, entre em contato com o administrador.'))
    }

    editUser = () => {
        this.setState({ isOpenedMenu: false })
        this.props.navigation.navigate('EditUser', {
            title: 'EDITAR USUÁRIO',
            id: this.props.navigation.getParam('id'),
            id_city: this.props.navigation.getParam('id_city'),
            city: this.props.navigation.getParam('city'),
            email: this.props.navigation.getParam('email'),
            name: this.props.navigation.getParam('name')
        })
    }
}

//-----------------------------------Styles---------------------------------------
const styles = StyleSheet.create({
    sideBar: {
        flex: 1,
        height: '100%',
        flexDirection: 'row'
    },
    container: {
        width: '85%',
        height: '100%',
        backgroundColor: '#424242',
        borderWidth: 1,
        borderColor: '#000',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    offset: {
        width: '15%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0)'
    },
    header: {
        width: '100%',
        height: '10%',
        backgroundColor: '#FFF',
        justifyContent: 'center',
        borderTopLeftRadius: 5
    },
    title: {
        textAlign: 'center',
        color: '#000',
        fontFamily: 'zcool',
        fontSize: Dimensions.get('window').width * 0.07
    },
    body: {
        width: '100%',
        flex: 1,
        alignItems: 'center'
    },
    backgroundImg: {
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: '30%',
        height: '18%',
        borderRadius: 50,
        backgroundColor: '#FFF'
    },
    profileImg: {
        width: '90%',
        height: '80%'
    },
    name: {
        paddingVertical: 5,
        textAlign: 'center',
        color: '#FFF',
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.04
    },
    subData: {
        paddingVertical: 5,
        textAlign: 'center',
        color: '#FFF',
        fontFamily: 'roboto-regular',
        fontSize: Dimensions.get('window').width * 0.035
    },
    button: {
        marginTop: 10,
        flexDirection: 'row',
        paddingVertical: 8,
        width: '60%',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    buttonLabel: {
        color: '#FFF',
        fontFamily: 'roboto-regular',
        fontSize: Dimensions.get('window').width * 0.04
    }
})
import React from 'react'
import { View, StyleSheet, TouchableOpacity, Dimensions, Text, TextInput } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'

export default class SearchBar extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            searchText: ''
        }
    }

    componentDidMount(){
        this.valueToSearch('')
    }

    //Incrementa ou decrementa o valor do estado de acordo com o parametro e envia ao Pai.
    async valueToSearch(value){

        if(value.search(/^[0-9]{1,10}[,]{0,1}[0-9]{1,2}$/) === -1 && value !== '') {
            await this.setState({ searchText: value })
            this.props.sendValueToParent(this.state.searchText)
        }
        else {
            
            await this.setState({ searchText: value })
            this.props.sendValueToParent(
                isNaN(parseFloat(this.state.searchText.replace(',', '.')).toString()) ? '' :
                parseFloat(this.state.searchText.replace(',', '.')).toString()
            )
        }
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.moneyBar}>
                    <Text style={styles.moneyLabel}>R$</Text>
                </View>
                <TextInput placeholder='Ex.: 22000,50' style={styles.input}
                    value={this.state.searchText} maxLength={20}
                    keyboardType={'phone-pad'}
                    onChangeText={(searchText) => this.setState({ searchText }) }/>
                <TouchableOpacity style={styles.button} onPress={() => 
                        this.valueToSearch(this.state.searchText)}>
                    <FontAwesome name="search" color="white"
                        size={Dimensions.get('window').width * 0.06}/>
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 10,
        width: '60%',
        height: '80%',
        backgroundColor: '#FFF'
    },
    moneyBar: {
        width: '15%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    moneyLabel: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.05,
        color: '#058E15'
    },
    input: {
        width: '70%',
        height: '100%',
        fontFamily: 'roboto-regular',
        fontSize: Dimensions.get('window').width * 0.04,
        color: '#000',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        paddingHorizontal: 10
    },
    button: {
        width: '15%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#058E15',
        borderBottomRightRadius: 9,
        borderTopRightRadius: 9
    }
})
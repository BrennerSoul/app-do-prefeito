import React from 'react'
import { View, StyleSheet, TouchableOpacity, Dimensions, TextInput } 
    from 'react-native'
import { FontAwesome } from '@expo/vector-icons'

export default class SearchBar extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            searchText: ''
        }
    }

    componentDidMount(){
        this.dataToSearch('')
    }

    //Incrementa ou decrementa o valor do estado de acordo com o parametro e envia ao Pai.
    async dataToSearch(data){

        let newData = ''

        if(data.search(/^[0-9]{1,10}[,]{0,1}[0-9]{1,2}$/) === -1 && data !== '') {
            if(data.includes('/') && data.includes(' ')){
                newData = data.split(' ')[0].split('/').reverse().join('-') + ' ' + 
                data.split(' ').slice(1).join(' ')
            } else 
            if(data.includes('/')){
                newData = data.split('/').reverse().join('-')
            }
            
            await this.setState({ searchText: data })
            this.props.sendDataToParent(newData)
        }
        else {
            
            await this.setState({ searchText: data })
            this.props.sendDataToParent(
                isNaN(parseFloat(this.state.searchText.replace(',', '.')).toString()) ? '' :
                parseFloat(this.state.searchText.replace(',', '.')).toString()
            )
        }
    }

    render(){
        return(
            <View style={[styles.container, this.props.size]}>
                <View style={styles.searchBar}>
                    <TextInput style={styles.input} placeholder='Pesquisar ...' 
                        value={this.state.searchText} maxLength={20}
                        keyboardType={this.props.history ? 'default' : 'phone-pad'}
                        onChangeText={(searchText) => this.setState({searchText})}/>
                </View>
                <TouchableOpacity style={styles.button} onPress={() => 
                    this.dataToSearch(this.state.searchText) }>
                    <FontAwesome name="search" color="white"
                        size={Dimensions.get('window').width * 0.06}/>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 10,
        width: '80%',
        height: '40%',
        backgroundColor: '#FFF'
    },
    searchBar: {
        flexDirection: 'row',
        width: '85%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        padding: 5
    },
    input: {
        width: '90%',
        fontFamily: 'roboto-regular',
        fontSize: Dimensions.get('window').width * 0.04,
        color: '#000'
    },
    button: {
        width: '15%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#058E15',
        borderBottomRightRadius: 9,
        borderTopRightRadius: 9
    }
})
import React from 'react'
import { View, StyleSheet, Dimensions, Text } from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import { FontAwesome } from '@expo/vector-icons'

export default class Ordenator extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            value: this.getHeaderTitles()[0].value,
            index: this.getHeaderTitles()[0].index
        }
    }

    //Edita as variáveis de estado e envia ao componente Pai.
    componentDidMount(){
        this.changePickerValue(this.state.value, this.state.index)
    }

    //Altera o valor do estado de acordo com o parametro e envia ao Pai.
    async changePickerValue(value, index){

        await this.setState({
            value: value ? value : this.getHeaderTitles()[0].value,
            index: index ? index : this.getHeaderTitles()[0].index
        })
        this.props.sendValueToParent(this.state.value, this.state.index)

    }

    //Cria o array de objetos pra enviar para o PickerSelect
    getHeaderTitles = () => {

        const headerTitles = []

        Object.keys(this.props.headerTitles).forEach((_, index) => {
            headerTitles.push(JSON.parse('{"label":"' + Object.values(this.props.headerTitles)[index] + 
            '", "value":"' + Object.keys(this.props.headerTitles)[index] + '", "index":"' + index + '"}'))
        })

        return headerTitles
    }

    render(){

        return(
            <View style={[styles.container, this.props.size]}>
                <Text style={styles.label}>{this.props.label}</Text>
                <View style={styles.select}>
                    <RNPickerSelect
                        disabled={this.props.disabled}
                        placeholder={{}}
                        items={this.getHeaderTitles()}
                        value={this.state.value}
                        onValueChange={(value, index) => this.changePickerValue(value, index) }
                        style={{
                            ...pickerSelectStyles,
                            iconContainer: {
                                top: 6,
                                right: 12
                            }
                        }}
                        doneText='OK'
                        useNativeAndroidPickerStyle={false}
                        Icon={() => {
                            return <FontAwesome name="caret-down" color="gray"
                                size={Dimensions.get('window').width * 0.06}/>
                        }}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '35%',
        flexDirection: 'row',
    },
    label: {
        width: '30%',
        textAlign: 'center',
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.035,
        color: '#FFF'
    },
    select: {
        width: '60%',
        height: '100%'
    }
})

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontFamily: 'roboto-regular',
        fontSize: Dimensions.get('window').width * 0.03,
        paddingVertical: 9,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 8,
        color: 'black',
        backgroundColor: '#FFF',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontFamily: 'roboto-regular',
        fontSize: Dimensions.get('window').width * 0.03,
        paddingHorizontal: 10,
        paddingVertical: 6,
        borderWidth: 1,
        borderRadius: 8,
        color: 'black',
        backgroundColor: '#FFF',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
})
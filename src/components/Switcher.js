import React from 'react'
import { View, StyleSheet, Dimensions, TouchableOpacity, Text, TextInput, Platform } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import moment from 'moment'

export default class SearchBar extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            searchText: moment().format("YYYY").toString()
        }
    }

    //Envia o valor do estado ao pai.
    componentDidMount(){
        this.changeValue('')
    }

    //Incrementa ou decrementa o valor do estado de acordo com o parametro e envia ao Pai.
    async changeValue(buttonSide){

        let value = parseInt(this.state.searchText)

        if(buttonSide === 'left') value--
        if(buttonSide === 'right') value++

        await this.setState({ searchText: value.toString() })
        this.props.sendYearToParent(this.state.searchText)
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.labelArea}>
                    <Text style={styles.label}>ANO</Text>
                </View>
                <View style={styles.switcherArea}>
                    <TouchableOpacity style={[styles.button, styles.leftButton]}
                        onPress={() => this.changeValue('left')}>
                        <FontAwesome name="chevron-left" color="white"
                            size={Dimensions.get('window').width * 0.045}/>
                    </TouchableOpacity>
                    <TextInput style={styles.input} value={this.state.searchText} maxLength={4} 
                        keyboardType={'numeric'}
                        onChangeText={(searchText) => this.setState({ searchText })}
                        onBlur={() => this.props.sendYearToParent(this.state.searchText)}/>
                    <TouchableOpacity style={[styles.button, styles.rightButton]}
                        onPress={() => this.changeValue('right')}>
                        <FontAwesome name="chevron-right" color="white"
                            size={Dimensions.get('window').width * 0.045}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        width: '30%',
        height: '100%',
        justifyContent: 'center'
    },
    labelArea: {
        width: '100%',
        height: '30%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.04,
        color: '#FFF'
    },
    switcherArea: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 10,
        width: '100%',
        height: '50%'
    },
    button: {
        width: '20%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#058E15'
    },
    leftButton: {
        borderBottomLeftRadius: 9,
        borderTopLeftRadius: 9
    },
    rightButton: {
        borderBottomRightRadius: 9,
        borderTopRightRadius: 9
    },
    input: {
        width: '60%',
        height: Platform.OS === 'android' ? '100%' : '95%',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        textAlign: 'center',
        backgroundColor: '#FFF',
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.035,
        color: '#000'
    }
})
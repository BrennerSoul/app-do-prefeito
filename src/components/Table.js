import React from 'react'
import { View, StyleSheet, Dimensions, Text, FlatList, TouchableWithoutFeedback } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import { strangers } from '../common'

export default class Table extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            resetExpand: true
        }
    }

    onPress = () => {
        this.state.resetExpand = true
    }

    render() {

        return (
            <View style={this.props.style}>
                <View style={styles.titleArea}>
                    <Text style={[styles.title, { color: this.props.data ? '#FFF' : '#F00' }]}>
                        {this.props.data ? this.props.title : 'DADOS NÃO ENCONTRADOS'}
                    </Text>
                </View>
                <FlatList data={this.props.data} keyExtractor={item => `${item._01_id}`}
                    refreshing={false} onRefresh={this.props.onRefresh}
                    renderItem={({ item }) =>
                        <TableItem resetExpand={this.state.resetExpand} item={item} title={item.subtitle}/>
                    }
                />
            </View>
        )
    }
}

class TableItem extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isExpanded: false
        }
    }

    componentWillReceiveProps() {
        this.setState({ isExpanded: !this.props.resetExpand })
    }

    toggleExpansion = () => {
        this.setState({ isExpanded: !this.state.isExpanded })
    }

    renderExpansableArea = () => {

        const header = Object.keys(this.props.item)
        const values = Object.values(this.props.item)
        const renderExpansableArea = []

        header.forEach((element, index) => {
            if (index > 0 && element.indexOf('_') === 0) {
                name = element.substring(4).split('_').join(' ')
                strangers.forEach(e => {
                    if (name.includes(e.code)) {
                        name = name.split(e.code).join(e.char)
                    }
                })
                renderExpansableArea.push(this.renderRow(index, name.split('w').join(''), values[index]))
            }
        })

        return renderExpansableArea
    }

    renderRow = (i, header, value) =>
        <View key={i} style={[styles.row, { backgroundColor: i % 2 === 0 ? '#FFF' : '#E6E6E6' }]}>
            <View style={{ width: '50%', borderRightWidth: 1, paddingLeft: 10, justifyContent: 'center' }}>
                <Text style={[styles.head, {
                    fontFamily:
                        header === 'Receita' || header === 'Despesa' || header === 'Gasto total'
                            ? 'roboto-bold' : 'roboto-regular',
                    color: header === 'Receita' ? '#013ADF' :
                        header === 'Despesa' ? '#FF0040' : '#000'
                }]}>
                    {header}
                </Text>
            </View>
            <View style={{ width: '50%', borderLeftWidth: 1, paddingRight: 10, justifyContent: 'center' }}>
                <Text style={[styles.rowData, {
                    fontFamily:
                        header === 'Receita' || header === 'Despesa' || header === 'Gasto total'
                            ? 'roboto-bold' : 'roboto-regular',
                    color: header === 'Receita' ? '#013ADF' :
                        header === 'Despesa' ? '#FF0040' : '#000'
                }]}>
                    {value}
                </Text>
            </View>
        </View>

    render() {

        return (
            <View>
                <TouchableWithoutFeedback onPress={this.toggleExpansion}>
                    <View style={styles.expandButton}>
                        <Text style={styles.rowTitle}>{this.props.title}</Text>
                        {this.state.isExpanded ?
                            <FontAwesome name='chevron-up' size={15} color='black' /> :
                            <FontAwesome name='chevron-down' size={15} color='black' />}
                    </View>
                </TouchableWithoutFeedback>
                {this.state.isExpanded ? this.renderExpansableArea() : null}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    titleArea: {
        backgroundColor: '#000',
        justifyContent: 'center',
        alignItems: 'center',
        height: Dimensions.get('window').height * 0.8 * 0.2 / 4
    },
    title: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.04,
        letterSpacing: 3
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        borderWidth: 1
    },
    expandButton: {
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FACB01',
        height: Dimensions.get('window').height * 0.8 * 0.2 / 4,
        paddingRight: 5
    },
    rowTitle: {
        flex: 1,
        textAlign: 'center',
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.035,
        color: '#000'
    },
    head: {
        fontFamily: 'roboto-regular',
        fontSize: Dimensions.get('window').width * 0.04,
        color: '#000'
    },
    rowData: {
        textAlign: 'right',
        fontFamily: 'roboto-regular',
        fontSize: Dimensions.get('window').width * 0.04,
        color: '#000'
    }
})
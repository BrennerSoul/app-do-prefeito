import React, { Component } from 'react'
import axios from 'axios'
import { View, ActivityIndicator, AsyncStorage } from 'react-native'

export default class AuthOrApp extends Component {

    static navigationOptions = { //Tira o cabeçalho do navigator desta tela
        header: null
    }

    componentWillMount = async () => {
        const json = await AsyncStorage.getItem('userData')
        const userData = JSON.parse(json) || {}

        if(userData.token){
            axios.defaults.headers['Authorization'] = `bearer ${userData.token}`
            this.props.navigation.navigate('ChooseMenu', userData)
        } else {
            this.props.navigation.navigate('Login')
        }
    }

    render(){
        return(
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' color='black'/>
            </View>
        )
    }

}
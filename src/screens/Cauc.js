//--------------------------------------Imports--------------------------------------------
import React from 'react';
import { View, BackHandler, ActivityIndicator, ImageBackground, WebView, Alert } from 'react-native'
import { LinearGradient } from 'expo'
import Menu from '../components/Menu'
import axios from 'axios'
import { getPayDayMessage, logout, server } from '../common'

export default class Cauc extends React.Component {

    //--------------------------------------Constructor--------------------------------------------
    constructor(props) {
        super(props)
        this.state = {
            data: undefined,
            loading: true,
            payDayMessage: undefined
        }
        this.mounted = false
    }

    //--------------------------------------API--------------------------------------------    
    async getData() {

        this.setState({ loading: true })
        await axios.get(`${server}/cauc/${this.props.navigation.getParam('id')}`)
            .then((response) => {
                this.setState({ data: response.data })
            },
                (reason) => this.setState({ data: reason })
            )
            .then(() => this.setState({ loading: false }))
            .catch(_ => Alert.alert('Erro ao buscar dados', 'Tente novamente mais tarde ou entre em contato com o administrador.'))
    }

    //--------------------------------------Lifecycles--------------------------------------------
    async componentDidMount() {
        this.mounted = true
        await getPayDayMessage(this.props.navigation.getParam('id'), this)
        this.getData()

        //Botao Back do Android
        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ChooseMenu', {
                title: 'APP DO PREFEITO',
                id_city: this.props.navigation.getParam('id_city'),
                id: this.props.navigation.getParam('id'),
                city: this.props.navigation.getParam('city'),
                email: this.props.navigation.getParam('email'),
                name: this.props.navigation.getParam('name')
            })
            return true
        })
    }

    //Remove a função
    componentWillUnmount() {
        this.mounted = false
        if (this.backHandler)
            this.backHandler.remove()
    }

    async componentDidUpdate(_, prevState) {
        if (prevState.payDayMessage !== this.state.payDayMessage) {
            await getPayDayMessage(this.props.navigation.getParam('id'), this)

            if (this.state.payDayMessage === '' && this.mounted)
                await logout(this.props.navigation)
        }
    }

    render() {

        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/img/modules_background.jpg')}
                resizeMode='stretch'>
                <LinearGradient style={{ flex: 1 }}
                    colors={['rgba(255,255,255,0.2)', 'rgba(255,255,255,0.2)']}>
                    <Menu navigation={this.props.navigation} excludeItem='CAUC' />
                    {this.state.loading ?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size='large' color='white' />
                        </View> :
                        <WebView
                            source={{ html: '<img src="data:image/jpeg;base64,' + this.state.data + '"/>' }}
                            originWhitelist={['*']}
                            domStorageEnabled={true}
                            scalesPageToFit
                            bounces={false}
                            style={{ flex: 1 }}
                        />}
                </LinearGradient>
            </ImageBackground>
        )
    }

}
//--------------------------------------Imports--------------------------------------------
import React from 'react';
import {
    StyleSheet, View, Text, Dimensions, ScrollView, BackHandler, ActivityIndicator,
    RefreshControl, ImageBackground, Alert
} from 'react-native'
import { LineChart, XAxis, BarChart } from 'react-native-svg-charts'
import { LinearGradient, Svg } from 'expo'
import Switcher from '../components/Switcher'
import Select from '../components/Select'
import Menu from '../components/Menu'
import axios from 'axios'
import { getPayDayMessage, logout, server, strangers } from '../common'
import moment from 'moment'
require('intl')
require('intl/locale-data/jsonp/pt-BR');

//--------------------------------------Selects--------------------------------------------
const selects = [
    {
        0: 'Todos', 1: 'Janeiro', 2: 'Fevereiro', 3: 'Março', 4: 'Abril', 5: 'Maio', 6: 'Junho', 7: 'Julho', 8: 'Agosto', 9: 'Setembro', 10: 'Outubro', 11: 'Novembro', 12: 'Dezembro'
    },
    {
        Financial: 'Receitas', Health: 'Saúde', Personal: 'Pessoal', Education: 'Educação', Fleet: 'Frota'
    }
]

export default class Charts extends React.Component {

    //--------------------------------------Constructor--------------------------------------------
    constructor(props) {
        super(props)
        this.state = {
            year: moment().format("YYYY").toString(),
            mes: '0',
            category: 'Financial',
            categoryIndex: 0,
            field: '',
            data: undefined,
            loading: true,
            payDayMessage: undefined
        }
        this.mounted = false
    }

    //--------------------------------------API--------------------------------------------    
    async getData() {

        const url = `m${this.state.category}`

        this.setState({ loading: true })
        await axios.post(`${server}/${url}`, {
            year: this.state.year,
            city: this.props.navigation.getParam('id_city'),
            search: '',
            orderBy: {
                field: 'mes',
                order: 'asc'
            }
        })
            .then((response) => this.setState({ data: response.data }),
                (_) => this.setState({ data: undefined }))
            .then(() => this.setState({ loading: false }))
            .catch(_ => Alert.alert('Erro ao buscar dados', 'Tente novamente mais tarde ou entre em contato com o administrador.'))

    }

    //--------------------------------------Lifecycles--------------------------------------------
    async componentDidMount() {
        this.mounted = true
        await getPayDayMessage(this.props.navigation.getParam('id'), this)
        this.getData()

        //Botao Back do Android
        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ChooseMenu', {
                title: 'APP DO PREFEITO',
                id_city: this.props.navigation.getParam('id_city'),
                id: this.props.navigation.getParam('id'),
                city: this.props.navigation.getParam('city'),
                email: this.props.navigation.getParam('email'),
                name: this.props.navigation.getParam('name')
            })
            return true
        })
    }

    //Remove a função
    componentWillUnmount() {
        this.mounted = false
        if (this.backHandler)
            this.backHandler.remove()
    }

    async componentDidUpdate(_, prevState) {
        if (prevState.payDayMessage !== this.state.payDayMessage) {
            await getPayDayMessage(this.props.navigation.getParam('id'), this)

            if (this.state.payDayMessage === '' && this.mounted)
                await logout(this.props.navigation)
        }

        if (prevState.year !== this.state.year ||
            prevState.mes !== this.state.mes ||
            prevState.category !== this.state.category ||
            prevState.field !== this.state.field)
            this.getData()
    }

    render() {

        const Decorator = ({ x, y, data }) => {
            return data.map((value, index) =>
                <Svg.G key={index} x={x(index)} y={y(value)}>
                    <Svg.Circle
                        r={4}
                        stroke={'rgb(134, 65, 244)'}
                        fill={'white'}
                    />
                    <Svg.G y={-10}>
                        <Svg.Text
                            alignmentBaseline={'middle'}
                            textAnchor={'middle'}
                            stroke={'rgb(255, 255, 255)'}
                            style={{ fontSize: 8 }}
                        >
                            {this.state.field.includes('Percentual') ?
                                `${data[index]}%`.replace('.', ',') :
                                this.state.categoryIndex == 0 || this.state.field === 'total' ||
                                    this.state.categoryIndex == 1 ||
                                    (this.state.categoryIndex == 2 && this.state.field === '_02_Gastos_com_pessoal') ||
                                    (this.state.categoryIndex == 3 && this.state.field.includes('Transporte')) ||
                                    this.state.categoryIndex == 4 ?
                                    new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                                        .format(data[index]) :
                                    data[index]}
                        </Svg.Text>
                    </Svg.G>
                </Svg.G>
            )
        }

        const Labels = ({ x, y, bandwidth, data }) => (
            data.map((value, index) => (
                <Svg.Text
                    key={index}
                    x={x(index) + (bandwidth / 2)}
                    y={y(value) - 10}
                    fontSize={10}
                    fill={'white'}
                    alignmentBaseline={'middle'}
                    textAnchor={'middle'}
                >
                    {new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value)}
                </Svg.Text>
            ))
        )

        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/img/modules_background.jpg')} resizeMode='stretch'>
                <LinearGradient style={styles.container}
                    colors={['rgba(255,255,255,0.2)', 'rgba(255,255,255,0.2)']}>
                    <Menu navigation={this.props.navigation} excludeItem='GRÁFICOS' />
                    <View style={{ width: '100%', flex: 1 }}>
                        <View style={styles.searchArea}>
                            <View style={{
                                flexDirection: 'row', justifyContent: 'space-around',
                                width: '100%', height: '30%', alignItems: 'center'
                            }}>
                                <Switcher sendYearToParent={this.getYearfromSwicher} />
                                <Select size={{ width: '60%', height: '80%' }} label='MÊS'
                                    headerTitles={selects[0]}
                                    sendValueToParent={this.getMonthfromSelect} />
                            </View>
                            <Select headerTitles={selects[1]} label='CATEGORIA'
                                size={{ height: '25%' }}
                                sendValueToParent={this.getCategoryfromSelect} />
                            <Select headerTitles={this.state.data ? this.state.data[3] : { a: '---' }} label='CAMPO'
                                size={{ height: '25%' }} disabled={false}
                                sendValueToParent={this.getFieldfromSelect} />
                        </View>
                        {this.state.loading ?
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator size='large' color='white' />
                            </View> :
                            <ScrollView refreshControl={
                                <RefreshControl refreshing={false} onRefresh={() => this.getData()} />
                            }>
                                {!this.state.data ?
                                    <Text style={styles.errorMessage}>Dados não existentes!</Text> : null}
                                {this.state.data && this.state.mes === '0' ?
                                    <View style={styles.chart}>
                                        <Text style={styles.chartTitle}>
                                            {selects[1][this.state.category]} –
                                    {` ${this.state.data ? this.state.data[3][this.state.field] : ''} `}
                                            de {this.state.year}
                                        </Text>
                                        <ScrollView bounces={false} horizontal>
                                            <View style={{
                                                height: 200, width: this.getDataByLineChart()[0] && this.getDataByLineChart()[0].length * 120 >= Dimensions.get('window').width ? 120 * this.getDataByLineChart()[0].length :
                                                    Dimensions.get('window').width,
                                                padding: 20, flexDirection: 'row'
                                            }}>
                                                <View style={{ flex: 1 }}>
                                                    <LineChart
                                                        style={{ flex: 1 }}
                                                        data={this.getDataByLineChart()[0]}
                                                        contentInset={{ top: 20, bottom: 10, left: 50, right: 50 }}
                                                        svg={{ stroke: 'rgb(0, 173, 0)' }}
                                                    >
                                                        <Decorator />
                                                    </LineChart>
                                                    <XAxis
                                                        style={{ marginHorizontal: -10, height: 30 }}
                                                        data={this.getDataByLineChart()[0]}
                                                        formatLabel={(_, index) => this.getDataByLineChart()[1][index]}
                                                        contentInset={{ left: 60, right: 60 }}
                                                        svg={{ fontSize: 10, fill: 'white' }}
                                                    />
                                                </View>
                                            </View>
                                        </ScrollView>
                                    </View> : null}
                                {this.state.data ?
                                    <View style={styles.chart}>
                                        <Text style={styles.chartTitle}>
                                            {selects[1][this.state.category]} – Balanço
                                                                        {selects[0][this.state.mes] !== 'Todos' ?
                                                ` de ${selects[0][this.state.mes]}` :
                                                ' anual'} de {this.state.year}
                                        </Text>
                                        <ScrollView bounces={false} horizontal>
                                            <View style={{
                                                flexDirection: 'row', height: 200,
                                                width: this.getDataByBarChart()[0] && this.getDataByBarChart()[0].length * 120 >= Dimensions.get('window').width ? 120 * this.getDataByBarChart()[0].length :
                                                    Dimensions.get('window').width, paddingVertical: 16
                                            }}>
                                                <View style={{ flex: 1 }}>
                                                    <BarChart
                                                        style={{ flex: 1, borderBottomWidth: 1 }}
                                                        data={this.getDataByBarChart()[0]}
                                                        svg={{ fill: 'rgb(0, 173, 0)' }}
                                                        contentInset={{ top: 20, bottom: 2, left: 30, right: 30 }}
                                                        spacingInner={0.5}
                                                    //minGrid={0}
                                                    >
                                                        <Labels />
                                                    </BarChart>
                                                    <XAxis
                                                        style={{ marginHorizontal: -10, height: 30 }}
                                                        data={this.getDataByBarChart()[0]}
                                                        formatLabel={(_, index) => this.getDataByBarChart()[1][index]}
                                                        contentInset={{ left: this.getDataByBarChart()[0] && this.getDataByBarChart()[0].length * 120 >= Dimensions.get('window').width ? 75 : (Dimensions.get('window').width / 2 + 10) / this.getDataByBarChart()[0].length, right: this.getDataByBarChart()[0] && this.getDataByBarChart()[0].length * 120 >= Dimensions.get('window').width ? 75 : (Dimensions.get('window').width / 2 + 10) / this.getDataByBarChart()[0].length }}
                                                        svg={{ fontSize: 10, fill: 'white' }}
                                                    />
                                                </View>
                                            </View>
                                        </ScrollView>
                                    </View> : null}
                            </ScrollView>}
                    </View>
                </LinearGradient>
            </ImageBackground>
        )
    }

    //-----------------------------------Methods to Parent---------------------------------------
    getYearfromSwicher = (year) => {
        this.setState({ year })
    }

    getMonthfromSelect = (mes) => {
        this.setState({ mes })
    }

    getCategoryfromSelect = (category, categoryIndex) => {
        this.setState({ category, categoryIndex })
    }

    getFieldfromSelect = (field) => {
        this.setState({ field })
    }

    //-----------------------------------Methods---------------------------------------
    getDataByBarChart = () => {

        if (this.state.mes === '0') {

            const monthsData = []
            const monthlabels = []

            if (this.state.data) {

                this.state.data[0].map((element, index) => {

                    const temp = []

                    Object.keys(element).forEach(field => {

                        if (field.substring(0, 1) === '_' && field !== '_01_id') {

                            if (typeof element[field] === 'string' && !element[field].includes('%')) {

                                if (index === 0) {
                                    name = field.substring(4).split('_').join(' ')
                                    strangers.forEach(e => {
                                        if (name.includes(e.code)) {
                                            name = name.split(e.code).join(e.char)
                                        }
                                    })
                                    monthlabels.push(name)
                                }

                                temp.push(parseFloat(element[field].substring(3).split('.').join('').replace(',', '.')))
                            }
                        }
                    })
                    monthsData.push(temp)
                })

                const sum = new Array(monthsData[0].length)
                sum.fill(0)

                monthsData.map((element) => {
                    element.map((e, i) => {
                        sum[i] += element[i]
                    })
                })
                return [sum, monthlabels]
            } else {
                return [[0], ['Sem dados']]
            }
        } else {

            //Traz os dados do mes correspondente ao select do mes
            const [mes] = this.state.data[0].filter((element) => element._01_id.toString().substring(0, 1) === this.state.mes)

            if (mes) {

                const monthData = []
                const monthlabels = []

                Object.keys(mes).forEach(field => {
                    if (field.substring(0, 1) === '_' && field !== '_01_id') {

                        if (typeof mes[field] === 'string' && !mes[field].includes('%')) {

                            name = field.substring(4).split('_').join(' ')
                            strangers.forEach(e => {
                                if (name.includes(e.code)) {
                                    name = name.split(e.code).join(e.char)
                                }
                            })
                            monthlabels.push(name)

                            monthData.push(parseFloat(mes[field].substring(3).split('.').join('').replace(',', '.')))
                        }
                    }
                })
                return [monthData, monthlabels]
            } else
                return [[0], ['Sem dados']]
        }
    }

    getDataByLineChart = () => {

        if (this.state.data) {

            const data = []
            const months = []

            this.state.data[0].map(element => {
                months.push(selects[0][element._01_id.toString().substring(0, element._01_id.toString().length - 5)])

                if (typeof element[this.state.field] === 'number') {
                    data.push(element[this.state.field])
                } else if (typeof element[this.state.field] === 'string') {
                    if (element[this.state.field].includes('%'))
                        data.push(parseFloat(element[this.state.field].replace(',', '.')))
                    else
                        data.push(parseFloat(element[this.state.field]
                            .substring(3).split('.').join('').replace(',', '.')))
                }
            })
            return [data, months]
        } else
            return [[0], ['']]

    }

}

//-----------------------------------Styles---------------------------------------
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    searchArea: {
        width: '100%',
        height: '25%',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderBottomWidth: 1
    },
    chart: {
        paddingTop: 10,
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#FFF',
        backgroundColor: '#2E2E2E'
    },
    chartTitle: {
        paddingVertical: 3,
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.04,
        color: '#FFF',
        textAlign: 'center'
    },
    errorMessage: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.05,
        color: 'red',
        textAlign: 'center',
        padding: 10
    }
})
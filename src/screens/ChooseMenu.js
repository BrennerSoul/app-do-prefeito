//--------------------------------------Imports--------------------------------------------
import React from 'react'
import { StyleSheet, View, Dimensions, BackHandler, TouchableWithoutFeedback, Platform, Alert, ActivityIndicator, Linking } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import Profile from '../components/Profile'
import Body from '../components/Body'
import { server, getPayDayMessage, logout } from '../common'
import { Notifications } from 'expo'
import axios from 'axios'

export default class ChooseMenu extends React.Component {

    //--------------------------------------Navigation Header---------------------------------------
    static navigationOptions = ({ navigation }) => ({
        headerLeft: <TouchableWithoutFeedback onPress={navigation.getParam('goToPdf')}>
            <View style={[styles.button, { marginLeft: 10, }]}>
                <FontAwesome name="file-pdf-o" color="white"
                    size={Dimensions.get('window').width * 0.08} />
            </View>
        </TouchableWithoutFeedback>,
        headerRight:
            <TouchableWithoutFeedback onPress={navigation.getParam('toggleProfileMenu')}>
                <View style={[styles.button, { marginRight: 10, }]}>
                    <FontAwesome name="user-circle" color="white"
                        size={Dimensions.get('window').width * 0.08} />
                </View>
            </TouchableWithoutFeedback>
    })

    //--------------------------------------Constructor--------------------------------------------
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            payDayMessage: undefined,
            data: undefined,
            clicked: false
        }
        this.mounted = false
    }

    //--------------------------------------API--------------------------------------------    
    async getData() {

        await axios.get(`${server}/pdf/${this.props.navigation.getParam('id_city')}/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTAyOX0.mY1SPIwLGvAfddEg93P8ut_efhaGJly8UevbQnKM3zE`)
            .then(_ => this.setState({ data: 'pdf' }),
                _ => {
                    this.setState({ data: undefined })
                    Alert.alert('PDF não encontrado.', 'Não há PDFs para este usuário!')
                }
            )
            .catch(_ => Alert.alert('Erro ao buscar dados do PDF', 'Tente novamente mais tarde ou entre em contato com o administrador.'))
    }

    //--------------------------------------Lifycycles--------------------------------------------
    async componentDidMount() {
        this.mounted = true
        this.props.navigation.setParams({ goToPdf: this.goToPDF })
        await getPayDayMessage(this.props.navigation.getParam('id'), this)
        this.setState({ loading: false })

        /*AppState.addEventListener('change', async (state) => {
            if (state === 'background' && this.state.payDayMessage === '')
                await logout(this.props.navigation)
        })*/

        if (Platform.OS === 'android') {
            Notifications.createChannelAndroidAsync('chat-messages', {
                name: 'Chat messages',
                sound: true,
            })
        }
        this._notificationSubscription = Notifications.addListener(this._handleNotification)

        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            BackHandler.exitApp()
            return true
        })
    }

    async componentDidUpdate(_, prevState) {

        if (prevState.payDayMessage !== this.state.payDayMessage) {
            await getPayDayMessage(this.props.navigation.getParam('id'), this)

            if (this.state.payDayMessage === '' && this.mounted)
                await logout(this.props.navigation)
        }

        if (this.state.data === 'pdf' && this.state.clicked) {
            this.setState({ clicked: false })
            Linking.openURL('http://www.appdoprefeito.com.br:3000/pdf/' + this.props.navigation.getParam('id_city') + '/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTAyOX0.mY1SPIwLGvAfddEg93P8ut_efhaGJly8UevbQnKM3zE')
        }
    }

    componentWillUnmount() {
        this.mounted = false
        if (this.backHandler)
            this.backHandler.remove()
    }

    render() {
        return (
            this.state.loading ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='large' color='black' />
                </View> :
                <View style={styles.container}>
                    <Profile navigation={this.props.navigation} />
                    <Body payDayMessage={this.state.payDayMessage} navigation={this.props.navigation} />
                </View>
        )
    }

    //--------------------------------------Functions--------------------------------------------
    _handleNotification = (notification) => {

        if (notification.data.moduleTitle === 'MENSAGEM') {

            if (notification.origin === 'selected')
                this.props.navigation.navigate('ChooseMenu', {
                    title: 'APP DO PREFEITO',
                    id_city: this.props.navigation.getParam('id_city'),
                    id: this.props.navigation.getParam('id'),
                    city: this.props.navigation.getParam('city'),
                    email: this.props.navigation.getParam('email'),
                    name: this.props.navigation.getParam('name')
                })

            Alert.alert(notification.data.title, notification.data.body, [
                {
                    text: 'OK', onPress: () => {
                        if (Platform.OS === 'android')
                            Notifications.dismissNotificationAsync(notification.notificationId)
                    }
                }
            ], { cancelable: false })
        } else {
            if (notification.origin === 'received') {

                Alert.alert('', `Novos dados inseridos no módulo ${notification.data.moduleTitle}`, [
                    {
                        text: `Ir para ${notification.data.moduleTitle}`,
                        onPress: () => {
                            if (Platform.OS === 'android')
                                Notifications.dismissNotificationAsync(notification.notificationId)
                            this.goToModule(notification)
                        }
                    },
                    {
                        text: 'Continuar aqui', onPress: () => {
                            if (Platform.OS === 'android')
                                Notifications.dismissNotificationAsync(notification.notificationId)
                        }
                    }
                ], { cancelable: false })
            } else if (notification.origin === 'selected')
                this.goToModule(notification)
        }
    }

    goToPDF = _ => {
        this.setState({ clicked: true })
        this.getData()
    }

    goToModule = (notification) => {

        this.props.navigation.navigate(notification.data.moduleName, {
            title: notification.data.moduleTitle,
            id_city: this.props.navigation.getParam('id_city'),
            id: this.props.navigation.getParam('id'),
            city: this.props.navigation.getParam('city'),
            email: this.props.navigation.getParam('email'),
            name: this.props.navigation.getParam('name')
        })

    }
}

//--------------------------------------Styles--------------------------------------------
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center'
    }
})
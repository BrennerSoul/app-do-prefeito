//--------------------------------------Imports--------------------------------------------
import React from 'react'
import {
    View, StyleSheet, Dimensions, Text, TextInput, BackHandler, ScrollView, KeyboardAvoidingView,
    TouchableHighlight, ActivityIndicator, Alert, ImageBackground, AsyncStorage
} from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation'
import Menu from '../components/Menu'
import axios from 'axios'
import { getPayDayMessage, logout, server } from '../common'
import { LinearGradient } from 'expo'
import { TextInputMask } from 'react-native-masked-text'

export default class Login extends React.Component {

    //--------------------------------------Constructor--------------------------------------------
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            confPassword: '',
            phone1: '',
            phone2: undefined,
            phone3: undefined,
            loading: true,
            payDayMessage: undefined
        }
        this.emailInput = React.createRef()
        this.passwordInput = React.createRef()
        this.confPasswordInput = React.createRef()
        this.phone1Input = React.createRef()
        this.phone2Input = React.createRef()
        this.phone3Input = React.createRef()
        this.mounted = false
    }

    //--------------------------------------API--------------------------------------------    
    async getUser() {

        this.setState({ loading: true })
        await axios.get(`${server}/user/${this.props.navigation.getParam('id')}`)
            .then((response) => this.setState({
                email: response.data.email,
                name: response.data.nome,
                phone1: response.data.telefones[0],
                phone2: response.data.telefones[1],
                phone3: response.data.telefones[2]
            }),
                (_) => Alert.alert('', 'Usuário não cadastrado!')
            )
            .then(() => this.setState({ loading: false }))
            .catch(_ => Alert.alert('Erro ao buscar usuário', 'Tente novamente mais tarde ou entre em contato com o administrador.'))

    }

    updateUser = async () => {

        this.setState({ loading: true })
        await axios.post(`${server}/user`, {
            id: this.props.navigation.getParam('id'),
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            telefone1: this.state.phone1,
            telefone2: this.state.phone2,
            telefone3: this.state.phone3
        })
            .then((_) => Alert.alert('', 'Usuário atualizado com sucesso!', [
                { text: 'OK', onPress: this.logout },
            ], { cancelable: false }),
                (_) => Alert.alert('', 'Usuário não atualizado! Tente novamente mais tarde.')
            )
            .then(() => this.setState({ loading: false }))
            .catch(_ => Alert.alert('Erro ao atualizar usuário', 'Tente novamente mais tarde ou entre em contato com o administrador.'))

    }

    //--------------------------------------Lifecycles--------------------------------------------
    async componentDidMount() {
        this.mounted = true
        await getPayDayMessage(this.props.navigation.getParam('id'), this)
        this.getUser()

        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ChooseMenu', {
                title: 'APP DO PREFEITO',
                id_city: this.props.navigation.getParam('id_city'),
                id: this.props.navigation.getParam('id'),
                city: this.props.navigation.getParam('city'),
                email: this.props.navigation.getParam('email'),
                name: this.props.navigation.getParam('name')
            })
            return true
        })
    }

    componentWillUnmount() {
        this.mounted = false
        if (this.backHandler)
            this.backHandler.remove()
    }

    async componentDidUpdate(_, prevState) {
        if (prevState.payDayMessage !== this.state.payDayMessage) {
            await getPayDayMessage(this.props.navigation.getParam('id'), this)

            if (this.state.payDayMessage === '' && this.mounted)
                await logout(this.props.navigation)
        }
    }

    render() {
        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/img/modules_background.jpg')}
                resizeMode='stretch'>
                <LinearGradient style={styles.container}
                    colors={['rgba(255,255,255,0.2)', 'rgba(255,255,255,0.2)']}>
                    <Menu navigation={this.props.navigation} excludeItem='EDITAR USUÁRIO' />
                    {this.state.loading ?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size='large' color='white' />
                        </View> :
                        <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={90}>
                            <ScrollView bounces={false} ref={this.scrollview}>
                                <View style={styles.form}>
                                    <Text style={styles.label}>Nome completo:</Text>
                                    <TextInput style={[styles.textInput, {
                                        borderColor:
                                            !this.state.name.trim() ? '#F00' : '#FFF'
                                    }]}
                                        textContentType='name'
                                        maxLength={254} value={this.state.name}
                                        onChangeText={(name) => this.setState({ name })}
                                        blurOnSubmit={false} returnKeyType='next' selectionColor='#FFF'
                                        onSubmitEditing={() => this.passwordInput.current.focus()} />
                                    <Text style={styles.label}>Senha:</Text>
                                    <TextInput style={styles.textInput} secureTextEntry selectionColor='#FFF'
                                        textContentType='password' maxLength={20} value={this.state.password}
                                        onChangeText={(password) => this.setState({ password })}
                                        ref={this.passwordInput} returnKeyType='next'
                                        onSubmitEditing={() => this.confPasswordInput.current.focus()} />
                                    <Text style={styles.label}>Confirmação da senha:</Text>
                                    <TextInput style={styles.textInput} secureTextEntry selectionColor='#FFF'
                                        textContentType='password' maxLength={20}
                                        value={this.state.confPassword}
                                        onChangeText={(confPassword) => this.setState({ confPassword })}
                                        ref={this.confPasswordInput} returnKeyType='next'
                                        onSubmitEditing={() => this.emailInput.current.focus()} />
                                    <Text style={styles.label}>E-mail:</Text>
                                    <TextInput style={[styles.textInput, {
                                        borderColor:
                                            !this.state.email.trim() ? '#F00' : '#FFF'
                                    }]}
                                        textContentType='emailAddress'
                                        maxLength={254} value={this.state.email}
                                        onChangeText={(email) => this.setState({ email })}
                                        ref={this.emailInput} keyboardType='email-address'
                                        blurOnSubmit={false} returnKeyType='next' selectionColor='#FFF'
                                        onSubmitEditing={() => this.phone1Input.current.focus()} />
                                    <Text style={styles.label}>Telefone 1:</Text>
                                    <TextInputMask type={'cel-phone'}
                                        options={{
                                            maskType: 'BRL',
                                            withDDD: true,
                                            dddMask: '(99) '
                                        }} style={[styles.textInput, {
                                            borderColor:
                                                !this.state.phone1.trim() ? '#F00' : '#FFF'
                                        }]}
                                        textContentType='telephoneNumber'
                                        maxLength={15} value={this.state.phone1}
                                        onChangeText={(phone1) => this.setState({ phone1 })}
                                        ref={this.phone1Input} keyboardType='numeric'
                                        blurOnSubmit={false} returnKeyType='next' selectionColor='#FFF'
                                        onSubmitEditing={() => this.phone2Input.current.focus()} />
                                    <Text style={styles.label}>Telefone 2:</Text>
                                    <TextInputMask type={'cel-phone'}
                                        options={{
                                            maskType: 'BRL',
                                            withDDD: true,
                                            dddMask: '(99) '
                                        }} style={styles.textInput} textContentType='telephoneNumber'
                                        maxLength={15} value={this.state.phone2}
                                        onChangeText={(phone2) => this.setState({ phone2 })}
                                        ref={this.phone2Input} keyboardType='numeric'
                                        blurOnSubmit={false} returnKeyType='next' selectionColor='#FFF'
                                        onSubmitEditing={() => this.phone3Input.current.focus()} />
                                    <Text style={styles.label}>Telefone 3:</Text>
                                    <TextInputMask type={'cel-phone'}
                                        options={{
                                            maskType: 'BRL',
                                            withDDD: true,
                                            dddMask: '(99) '
                                        }} style={styles.textInput} textContentType='telephoneNumber'
                                        maxLength={15} value={this.state.phone3}
                                        onChangeText={(phone3) => this.setState({ phone3 })}
                                        ref={this.phone3Input} keyboardType='numeric'
                                        blurOnSubmit={false} returnKeyType='go' selectionColor='#FFF'
                                        onSubmitEditing={this.sendData} />
                                </View>
                                <View style={styles.buttonArea}>
                                    <TouchableHighlight underlayColor='rgba(250,183,0, 0.5)'
                                        style={styles.button} onPress={this.validateEdit}>
                                        <Text style={styles.buttonLabel}>ENVIAR</Text>
                                    </TouchableHighlight>
                                </View>
                            </ScrollView>
                        </KeyboardAvoidingView>}
                </LinearGradient>
            </ImageBackground>
        )
    }

    //-----------------------------------Methods---------------------------------------
    logout = async () => {

        await axios.post(`${server}/user/insert`, {
            id: this.props.navigation.getParam('id'),
            token: ''
        })
            .then(_ => false, (reason) => Alert.alert(reason))
            .then(() => {
                delete axios.defaults.headers.common['Authorization']
                AsyncStorage.removeItem('userData')
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Loading' })]
                })
                this.props.navigation.dispatch(resetAction)
            })
            .catch(_ => Alert.alert('Erro ao sair do sistema', 'Entre em contato com o administrador.'))

    }

    validateEdit = () => {
        if (!this.state.name.trim() || !this.state.email.trim() || !this.state.phone1.trim())
            Alert.alert('', 'Preencha todos os campos obrigatórios!')
        else {
            if (this.state.password !== this.state.confPassword)
                Alert.alert('', 'Senha e confirmação da senha devem ser iguais!')
            else
                this.updateUser()
        }

    }
}

//-----------------------------------Styles---------------------------------------
const styles = StyleSheet.create({
    container: {
        flex: 1 //100% da tela
    },
    form: {
        paddingVertical: 20,
        flex: 1,
        justifyContent: 'center',
        width: '100%',
        alignItems: 'center'
    },
    label: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.04,
        color: '#FFF',
        width: '80%',
        textAlign: 'left',
        marginBottom: 10
    },
    textInput: {
        fontFamily: 'roboto-regular',
        color: '#E6E6E6',
        borderColor: '#FFF',
        borderBottomWidth: 1,
        width: '80%',
        marginBottom: 10,
        fontSize: Dimensions.get('window').width * 0.05
    },
    buttonArea: {
        height: Dimensions.get('window').height * 0.15,
        width: '100%',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    button: {
        borderRadius: 10,
        borderColor: '#0E0233',
        borderWidth: 2,
        height: '40%',
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FAB700'
    },
    buttonLabel: {
        fontFamily: 'roboto-bold',
        color: '#0E0233',
        fontSize: Dimensions.get('window').width * 0.06
    }
})
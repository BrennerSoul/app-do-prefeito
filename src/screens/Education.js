//--------------------------------------Imports--------------------------------------------
import React from 'react';
import {
    StyleSheet, View, Text, TouchableOpacity, Dimensions, BackHandler, ActivityIndicator,
    ImageBackground, Alert
} from 'react-native'
import { LinearGradient } from 'expo'
import Table from '../components/Table'
import SearchBar from '../components/SearchBar'
import SearchBar2 from '../components/SearchBar2'
import Switcher from '../components/Switcher'
import Ordenator from '../components/Ordenator'
import Menu from '../components/Menu'
import axios from 'axios'
import { getPayDayMessage, logout, server } from '../common'
import moment from 'moment'

export default class Education extends React.Component {

    //--------------------------------------Constructor--------------------------------------------
    constructor(props) {
        super(props)
        this.state = {
            year: moment().format("YYYY").toString(),
            fieldToOrder: '_mes',
            orderDirection: 'desc',
            searchValue: '',
            searchData: '',
            isMonthly: true,
            data: undefined,
            loading: true,
            payDayMessage: undefined
        }
        this.mounted = false
    }

    //--------------------------------------API--------------------------------------------    
    async getData(loading4ever) {

        const url = this.state.isMonthly ? 'mEducation' : 'yEducation'
        let fieldToOrder = undefined

        if (this.state.fieldToOrder[0] === '_')
            fieldToOrder = this.state.fieldToOrder.substr(1)
        else
            fieldToOrder = this.state.fieldToOrder

        this.setState({ loading: true })
        await axios.post(`${server}/${url}`, {
            year: this.state.year,
            city: this.props.navigation.getParam('id_city'),
            search: this.state.isMonthly ? this.state.searchValue : this.state.searchData,
            orderBy: {
                field: fieldToOrder,
                order: this.state.orderDirection
            }
        }).then((response) => this.setState({ data: response.data }),
            (_) => this.setState({ data: undefined }))
            .then(() => loading4ever ? false : this.setState({ loading: false }))
            .catch(_ => Alert.alert('Erro ao buscar dados', 'Tente novamente mais tarde ou entre em contato com o administrador.'))
    }

    //--------------------------------------Lifecycles--------------------------------------------
    async componentDidMount() {
        this.mounted = true
        await getPayDayMessage(this.props.navigation.getParam('id'), this)
        this.getData()

        //Botao Back do Android
        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ChooseMenu', {
                title: 'APP DO PREFEITO',
                id_city: this.props.navigation.getParam('id_city'),
                id: this.props.navigation.getParam('id'),
                city: this.props.navigation.getParam('city'),
                email: this.props.navigation.getParam('email'),
                name: this.props.navigation.getParam('name')
            })
            return true
        })
    }

    //Remove a função
    componentWillUnmount() {
        this.mounted = false
        if (this.backHandler)
            this.backHandler.remove()
    }

    async componentDidUpdate(_, prevState) {
        if (prevState.payDayMessage !== this.state.payDayMessage) {
            await getPayDayMessage(this.props.navigation.getParam('id'), this)

            if (this.state.payDayMessage === '' && this.mounted)
                await logout(this.props.navigation)
        }

        if (prevState.isMonthly !== this.state.isMonthly)
            this.getData(true)

        if (prevState.year !== this.state.year ||
            prevState.fieldToOrder !== this.state.fieldToOrder ||
            prevState.orderDirection !== this.state.orderDirection ||
            prevState.searchValue !== this.state.searchValue ||
            prevState.searchData !== this.state.searchData)
            this.getData()
    }

    render() {

        const opacity = (condition) => condition ? 0.5 : 1

        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/img/modules_background.jpg')}
                resizeMode='stretch'>
                <LinearGradient style={styles.container}
                    colors={['rgba(255,255,255,0.2)', 'rgba(255,255,255,0.2)']}>
                    <Menu navigation={this.props.navigation} excludeItem='EDUCAÇÃO' />
                    <View style={styles.buttonArea}>
                        <Text style={styles.buttonAreaTitle}>TABELAS:</Text>
                        <TouchableOpacity
                            onPress={() => this.setState({ isMonthly: true })}
                            style={[styles.button, { opacity: opacity(this.state.isMonthly) }]}
                            disabled={this.state.isMonthly}>
                            <Text style={styles.buttonLabel}>Mensal</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.setState({ isMonthly: false })}
                            style={[styles.button, { opacity: opacity(!this.state.isMonthly) }]}
                            disabled={!this.state.isMonthly}>
                            <Text style={styles.buttonLabel}>Anual</Text>
                        </TouchableOpacity>
                    </View>
                    {this.state.isMonthly ?
                        <View style={{ width: '100%', flex: 1 }}>
                            <View style={styles.searchArea}>
                                <View style={{
                                    flexDirection: 'row', justifyContent: 'space-around',
                                    width: '100%', height: '45%', alignItems: 'center'
                                }}>
                                    <Switcher sendYearToParent={this.getYearfromSwicher} />
                                    <SearchBar sendValueToParent={this.getValueFromSearchBar} />
                                </View>
                                <Ordenator headerTitles={this.state.data ? this.state.data[1] : { desc_mes: '' }} sendOrderByToParent={this.getOrderByfromOrdenator} />
                            </View>
                            {this.state.loading ?
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator size='large' color='white' />
                                </View> :
                                <Table title={this.state.data ? this.state.data[2] : ''} style={{ flex: 1 }}
                                    data={this.state.data ? this.state.data[0] : null} onRefresh={() => this.getData()} />}
                        </View> :
                        <View style={{ width: '100%', flex: 1 }}>
                            <View style={styles.searchArea}>
                                <SearchBar2 sendDataToParent={this.getDataFromSearchBar2} />
                                <Ordenator headerTitles={this.state.data ? this.state.data[1] : { desc_ano: '' }} sendOrderByToParent={this.getOrderByfromOrdenator} />
                            </View>
                            {this.state.loading ?
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator size='large' color='white' />
                                </View> :
                                <Table title={this.state.data ? this.state.data[2] : ''} style={{ flex: 1 }} data={this.state.data ? this.state.data[0] : null} onRefresh={() => this.getData()} />}
                        </View>}
                </LinearGradient>
            </ImageBackground>
        )
    }

    //-----------------------------------Methods to Parent---------------------------------------
    getYearfromSwicher = (year) => {
        this.setState({ year })
    }

    getOrderByfromOrdenator = (fieldToOrder, orderDirection) => {
        this.setState({ fieldToOrder, orderDirection })
    }

    getValueFromSearchBar = (searchValue) => {
        this.setState({ searchValue })
    }

    getDataFromSearchBar2 = (searchData) => {
        this.setState({ searchData })
    }

}

//-----------------------------------Styles---------------------------------------
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    buttonArea: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: '100%',
        height: '10%',
        borderBottomWidth: 1,
        backgroundColor: 'rgba(14,75,141,0.7)'
    },
    buttonAreaTitle: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.04,
        color: '#FFF'
    },
    button: {
        borderColor: '#000',
        borderWidth: 1,
        borderRadius: 15,
        width: '25%',
        height: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FACB01'
    },
    buttonLabel: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.04,
        color: '#000'
    },
    searchArea: {
        width: '100%',
        height: '20%',
        justifyContent: 'space-around',
        alignItems: 'center'
    }
})
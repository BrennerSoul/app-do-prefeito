//--------------------------------------Imports--------------------------------------------
import React from 'react';
import { StyleSheet, View, BackHandler, ActivityIndicator, ImageBackground, Alert } from 'react-native'
import { LinearGradient } from 'expo'
import Table from '../components/Table'
import SearchBar2 from '../components/SearchBar2'
import Ordenator from '../components/Ordenator'
import Menu from '../components/Menu'
import axios from 'axios'
import { getPayDayMessage, logout, server } from '../common'

export default class History extends React.Component {

    //--------------------------------------Constructor--------------------------------------------
    constructor(props) {
        super(props)
        this.state = {
            fieldToOrder: '_data_registro',
            orderDirection: 'desc',
            searchData: '',
            data: undefined,
            loading: true,
            payDayMessage: undefined
        }
        this.mounted = false
    }

    //--------------------------------------API--------------------------------------------    
    async getData() {

        let fieldToOrder = undefined

        if (this.state.fieldToOrder[0] === '_')
            fieldToOrder = this.state.fieldToOrder.substr(1)
        else
            fieldToOrder = this.state.fieldToOrder

        this.setState({ loading: true })
        await axios.post(`${server}/history`, {
            city: this.props.navigation.getParam('id_city'),
            search: this.state.searchData,
            orderBy: {
                field: fieldToOrder,
                order: this.state.orderDirection
            }
        }).then((response) => this.setState({ data: response.data }),
            (_) => this.setState({ data: undefined }))
            .then(() => this.setState({ loading: false }))
            .catch(_ => Alert.alert('Erro ao buscar dados', 'Tente novamente mais tarde ou entre em contato com o administrador.'))

    }

    //--------------------------------------Lifecycles--------------------------------------------
    async componentDidMount() {
        this.mounted = true
        await getPayDayMessage(this.props.navigation.getParam('id'), this)
        this.getData()

        //Botao Back do Android
        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ChooseMenu', {
                title: 'APP DO PREFEITO',
                id_city: this.props.navigation.getParam('id_city'),
                id: this.props.navigation.getParam('id'),
                city: this.props.navigation.getParam('city'),
                email: this.props.navigation.getParam('email'),
                name: this.props.navigation.getParam('name')
            })
            return true
        })
    }

    //Remove a função
    componentWillUnmount() {
        this.mounted = false
        if (this.backHandler)
            this.backHandler.remove()
    }

    async componentDidUpdate(_, prevState) {
        if (prevState.payDayMessage !== this.state.payDayMessage) {
            await getPayDayMessage(this.props.navigation.getParam('id'), this)

            if (this.state.payDayMessage === '' && this.mounted)
                await logout(this.props.navigation)
        }

        if (prevState.fieldToOrder !== this.state.fieldToOrder ||
            prevState.orderDirection !== this.state.orderDirection ||
            prevState.searchData !== this.state.searchData)
            this.getData()
    }

    render() {

        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/img/modules_background.jpg')}
                resizeMode='stretch'>
                <LinearGradient style={styles.container}
                    colors={['rgba(255,255,255,0.2)', 'rgba(255,255,255,0.2)']}>
                    <Menu navigation={this.props.navigation} excludeItem='HISTÓRICO' />
                    <View style={{ width: '100%', flex: 1 }}>
                        <View style={styles.searchArea}>
                            <SearchBar2 sendDataToParent={this.getDataFromSearchBar2} history />
                            <Ordenator headerTitles={this.state.data ? this.state.data[1] : { desc_data_registro: '' }}
                                sendOrderByToParent={this.getOrderByfromOrdenator} />
                        </View>
                        {this.state.loading ?
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator size='large' color='white' />
                            </View> :
                            <Table title={this.state.data ? this.state.data[2] : ''} style={{ flex: 1 }}
                                data={this.state.data ? this.state.data[0] : null} onRefresh={() => this.getData()} />}
                    </View>
                </LinearGradient>
            </ImageBackground>
        )
    }

    //-----------------------------------Methods to Parent---------------------------------------
    getOrderByfromOrdenator = (fieldToOrder, orderDirection) => {
        this.setState({ fieldToOrder, orderDirection })
    }

    getDataFromSearchBar2 = (searchData) => {
        this.setState({ searchData })
    }

}

//-----------------------------------Styles---------------------------------------
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    searchArea: {
        width: '100%',
        height: '20%',
        justifyContent: 'space-around',
        alignItems: 'center'
    }
})
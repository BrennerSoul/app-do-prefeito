import React from 'react'
import {
    View, StyleSheet, Image, KeyboardAvoidingView, TouchableHighlight, TouchableOpacity,
    Dimensions, Text, TextInput, ImageBackground, BackHandler, AsyncStorage, ScrollView,
    ActivityIndicator, Alert, Platform
} from 'react-native'
import axios from 'axios'
import CreditsModal from '../components/CreditsModal'
import { server } from '../common'
import { LinearGradient, Permissions, Notifications } from 'expo'

export default class Login extends React.Component {

    static navigationOptions = { //Tira o cabeçalho do navigator desta tela
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            modalVisibility: false,
            loading: true
        }
        this.passwordInput = React.createRef()
    }

    async componentDidMount() {
        this.setState({ loading: false })
        //Botão back do Android
        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            BackHandler.exitApp()
            return true
        })
    }

    /* Usado pra testes de notificação
    _handleNotification = (notification) => {
        console.log(notification)
    }*/

    componentWillUnmount() {
        if (this.backHandler)
            this.backHandler.remove()
    }

    registerForPushNotificationsAsync = async id => {

        const { status: existingStatus } = await Permissions.getAsync(
            Permissions.NOTIFICATIONS
        )
        let finalStatus = existingStatus;

        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        // Stop here if the user did not grant permissions
        if (finalStatus !== 'granted') {
            return;
        }

        // Get the token that uniquely identifies this device
        let token = await Notifications.getExpoPushTokenAsync()

        await axios.post(`${server}/user/insert`, {
            id,
            token
        })
            .catch(_ => Alert.alert('Aviso sobre notificações', 'O aplicativo não conseguiu habilitar as notificações para este usuário. Por favor, entre em contato com o administrador se deseja receber notificações.'))
    }

    signin = async () => {

        try {
            this.setState({ loading: true })
            const res = await axios.post(`${server}/signin`, {
                username: this.state.username,
                password: this.state.password
            })

            axios.defaults.headers.common['Authorization'] = `bearer ${res.data.token}`
            AsyncStorage.setItem('userData', JSON.stringify(res.data))
            await this.registerForPushNotificationsAsync(res.data.id)
            this.props.navigation.navigate('ChooseMenu', res.data)
            this.setState({ loading: false })

        } catch (err) {
            Alert.alert('Dados incorretos!')
            this.setState({ loading: false })
        }

    }

    goToForgotPasswordScreen = () => {
        this.setState({ username: undefined, password: undefined })
        this.props.navigation.navigate('PasswordRecovery', { title: 'RECUPERAR SENHA' })
    }

    openModal = () => {
        this.setState({ modalVisibility: true })
    }

    render() {
        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/img/city_hall.jpg')}
                resizeMode='stretch'>
                <LinearGradient style={styles.container}
                    colors={['rgba(0,130,200,0.5)', 'rgba(102,125,182,0.5)']}>
                    <CreditsModal isVisible={this.state.modalVisibility}
                        onCancel={() => this.setState({ modalVisibility: false })} />
                    {this.state.loading ?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size='large' color='white' />
                        </View> :
                        <ScrollView bounces={false} contentContainerStyle={{ flex: 1 }}>
                            <KeyboardAvoidingView behavior='padding' style={styles.avoidingView}>
                                <Image style={styles.logo} source={require('../../assets/img/logo.png')}
                                    resizeMode='stretch' borderRadius={20} />
                                <View style={styles.form}>
                                    <Text style={styles.label}>Nome de usuário:</Text>
                                    <TextInput style={styles.textInput} textContentType='username'
                                        maxLength={30} value={this.state.username}
                                        onChangeText={(username) => this.setState({ username })}
                                        blurOnSubmit={false} returnKeyType='next'
                                        onSubmitEditing={() => this.passwordInput.current.focus()} />
                                    <Text style={styles.label}>Senha:</Text>
                                    <TextInput style={styles.textInput} secureTextEntry
                                        textContentType='password' maxLength={20} value={this.state.password}
                                        onChangeText={(password) => this.setState({ password })}
                                        ref={this.passwordInput} returnKeyType='go'
                                        onSubmitEditing={this.signin} />
                                </View>
                            </KeyboardAvoidingView>
                            <View style={styles.buttonArea}>
                                <TouchableHighlight underlayColor='rgba(250,183,0, 0.5)' style={styles.button}
                                    onPress={this.signin}>
                                    <Text style={styles.buttonLabel}>ENTRAR</Text>
                                </TouchableHighlight>
                                <View style={styles.forgotArea}>
                                    <TouchableOpacity onPress={this.goToForgotPasswordScreen}>
                                        <Text style={styles.forgotLabel}>Esqueceu sua senha?</Text>
                                    </TouchableOpacity>
                                </View>
                                {Platform.OS === 'android' ?
                                    <TouchableOpacity onPress={this.openModal}>
                                        <Text style={styles.forgotLabel}>Desenvolvedores</Text>
                                    </TouchableOpacity> : null}
                            </View>
                        </ScrollView>}
                </LinearGradient>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1 //100% da tela
    },
    avoidingView: {
        width: '100%',
        height: '75%',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    logo: {
        width: '80%',
        height: '45%'
    },
    form: {
        justifyContent: 'center',
        height: '30%',
        width: '90%',
        alignItems: 'center'
    },
    label: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.045,
        color: '#FFF',
        width: '80%',
        textAlign: 'left',
        marginBottom: 10
    },
    textInput: {
        fontFamily: 'roboto-regular',
        color: '#E6E6E6',
        borderColor: '#FFF',
        borderBottomWidth: 1,
        width: '80%',
        marginBottom: 10,
        fontSize: Dimensions.get('window').width * 0.045
    },
    buttonArea: {
        height: '25%',
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    button: {
        borderRadius: 10,
        borderColor: '#0E0233',
        borderWidth: 2,
        height: '30%',
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FAB700'
    },
    buttonLabel: {
        fontFamily: 'roboto-bold',
        color: '#0E0233',
        fontSize: Dimensions.get('window').width * 0.06
    },
    forgotArea: {
        height: '40%',
        justifyContent: 'center'
    },
    forgotLabel: {
        fontFamily: 'roboto-bold',
        color: '#2E2E2E',
        fontSize: Dimensions.get('window').width * 0.045,
        textAlign: 'center',
        textDecorationLine: 'underline'
    }
})
import React from 'react'
import { WebView, View, ActivityIndicator, TouchableWithoutFeedback, Dimensions, BackHandler }
    from 'react-native'
import { Feather } from '@expo/vector-icons'
import { StackActions, NavigationActions } from 'react-navigation'

export default class MiniBrowser extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerRight: <View />,
        headerLeft: <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
            <View style={{ paddingHorizontal: 8 }}>
                <Feather name="arrow-left" color="white"
                    size={Dimensions.get('window').width * 0.06} />
            </View>
        </TouchableWithoutFeedback>
    })

    constructor(props) {
        super(props)
        this.state = {
            url: this.props.navigation.getParam('url'),
            loading: true
        }
    }

    async componentDidMount() {
        //Botao Back do Android
        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack()
            return true
        })
    }

    //Remove a função
    componentWillUnmount() {
        if (this.backHandler)
            this.backHandler.remove()
    }

    onNavigationStateChange = navState => {
        if (navState.url.substring(0, navState.url.indexOf('.br')) !==
            this.state.url.substring(0, this.state.url.indexOf('.br')))
            this.props.navigation.goBack()
        else {
            this.setState({ loading: false })
        }
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                {this.state.loading ?
                    <View style={{
                        height: '100%', width: '100%', justifyContent: 'center',
                        alignItems: 'center', backgroundColor: 'white'
                    }} zIndex={1} position='absolute'>
                        <ActivityIndicator size='large' />
                    </View> : null}
                <WebView
                    source={{ uri: this.state.url }}
                    domStorageEnabled={true}
                    onNavigationStateChange={this.onNavigationStateChange}
                    startInLoadingState
                    scalesPageToFit
                    javaScriptEnabled
                    bounces={false}
                    style={{ flex: 1 }}
                />
            </View>
        )
    }
}
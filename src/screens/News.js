//--------------------------------------Imports--------------------------------------------
import React from 'react';
import {
    FlatList, View, BackHandler, ActivityIndicator, StyleSheet, Dimensions, Text,
    TouchableOpacity, ImageBackground, Alert
} from 'react-native';
import { LinearGradient } from 'expo'
import Notice from '../components/Notice'
import Menu from '../components/Menu'
import axios from 'axios'
import { getPayDayMessage, logout, server } from '../common'

export default class News extends React.Component {

    //--------------------------------------Constructor--------------------------------------------
    constructor(props) {
        super(props)
        this.state = {
            data: undefined,
            loading: true,
            payDayMessage: undefined
        }
        this.mounted = false
    }

    //--------------------------------------API--------------------------------------------    
    async getData() {

        this.setState({ loading: true })
        await axios.get(`${server}/news`)
            .then((response) => this.setState({ data: response.data }),
                (_) => this.setState({ data: undefined }))
            .then(() => this.setState({ loading: false }))
            .catch(_ => Alert.alert('Erro ao buscar dados', 'Tente novamente mais tarde ou entre em contato com o administrador.'))

    }

    //--------------------------------------Lifecycles--------------------------------------------
    async componentDidMount() {
        this.mounted = true
        await getPayDayMessage(this.props.navigation.getParam('id'), this)
        this.getData()

        //Botao Back do Android
        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ChooseMenu', {
                title: 'APP DO PREFEITO',
                id_city: this.props.navigation.getParam('id_city'),
                id: this.props.navigation.getParam('id'),
                city: this.props.navigation.getParam('city'),
                email: this.props.navigation.getParam('email'),
                name: this.props.navigation.getParam('name')
            })
            return true
        })
    }

    componentWillUnmount() {
        this.mounted = false
        if (this.backHandler)
            this.backHandler.remove()
    }

    async componentDidUpdate(_, prevState) {
        if (prevState.payDayMessage !== this.state.payDayMessage) {
            await getPayDayMessage(this.props.navigation.getParam('id'), this)

            if (this.state.payDayMessage === '' && this.mounted)
                await logout(this.props.navigation)
        }
    }

    render() {

        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/img/news_background.jpg')}
                resizeMode='stretch'>
                <LinearGradient style={{ flex: 1 }}
                    colors={['rgba(255,255,255,0.5)', 'rgba(255,255,255,0.5)']}>
                    <Menu navigation={this.props.navigation} excludeItem='NOTÍCIAS' />
                    <View style={styles.fontArea}>
                        <Text style={styles.label}>Fontes:</Text>
                        <View style={styles.linkArea}>
                            <TouchableOpacity onPress={() => this.goToSite('https://www.cnm.org.br')}>
                                <Text style={styles.link}>CNM</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.goToSite('http://amvale.org.br')}>
                                <Text style={styles.link}>Amvale</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.goToSite('https://portalamm.org.br')}>
                                <Text style={styles.link}>AMM</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {this.state.loading ?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size='large' color='white' />
                        </View> :
                        <FlatList data={this.state.data} keyExtractor={item => `${item.id}`}
                            refreshing={false} onRefresh={() => this.getData()}
                            renderItem={({ item }) =>
                                <Notice {...item} navigation={this.props.navigation} />
                            } />}
                </LinearGradient>
            </ImageBackground>
        )
    }

    //-----------------------------------Methods---------------------------------------
    goToSite = (url) => {
        this.props.navigation.navigate('MiniBrowser', {
            title: 'NAVEGADOR',
            id_city: this.props.navigation.getParam('id_city'),
            id: this.props.navigation.getParam('id'),
            city: this.props.navigation.getParam('city'),
            email: this.props.navigation.getParam('email'),
            name: this.props.navigation.getParam('name'),
            url
        })
    }

}

//-----------------------------------Styles---------------------------------------
const styles = StyleSheet.create({
    link: {
        fontFamily: 'roboto-bold',
        color: '#00BFFF',
        fontSize: Dimensions.get('window').width * 0.045,
        textAlign: 'center',
        textDecorationLine: 'underline'
    },
    fontArea: {
        flexDirection: 'row',
        height: Dimensions.get('window').height * 0.05,
        width: Dimensions.get('window').width,
        borderBottomWidth: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    label: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.05,
        color: '#FFF'
    },
    linkArea: {
        flexDirection: 'row',
        width: '80%',
        justifyContent: 'space-around'
    }
})
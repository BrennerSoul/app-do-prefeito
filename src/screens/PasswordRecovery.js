import React from 'react';
import { View, ScrollView, TextInput, Text, BackHandler, Dimensions, TouchableHighlight,
    TouchableWithoutFeedback, ActivityIndicator, ImageBackground, StyleSheet, Alert } from 'react-native'
import { LinearGradient } from 'expo'
import { FontAwesome, Feather } from '@expo/vector-icons'
import axios from 'axios'
import qs from 'qs'

export default class PasswordRecovery extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        headerRight: <View/>,
        headerLeft: <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
                        <View style={{ paddingHorizontal: 8 }}>
                            <Feather name="arrow-left" color="white" 
                                size={Dimensions.get('window').width * 0.06}/>
                        </View>
                    </TouchableWithoutFeedback>
    })

    constructor(props){
        super(props)
        this.state = {
            email: undefined,
            error: undefined,
            loading: true
        }
    }

    async componentDidMount(){
        this.setState({ loading: false })
        //Botao Back do Android
        this.backHandler = await BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack()
            return true
        })
    }

    //Remove a função
    componentWillUnmount() {
        if(this.backHandler)
            this.backHandler.remove()
    }

    emailIsValid = (email) => {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
    }

    sendMail = async () => {

        if(this.emailIsValid(this.state.email)){

            this.setState({ loading: true })
            await axios.post('http://www.appdoprefeito.com.br/enviaEmail', 
                qs.stringify({ email: this.state.email }),
                { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
            )
            .then((response) => this.setState({ error: response.data.substring(
                    response.data.indexOf('<strong id="mensagem">') + '<strong id="mensagem">'.length,
                    response.data.indexOf('</strong>')), email: undefined 
                }),
                (_) => this.setState({ error: undefined, email: undefined })
            )
            .then(() => this.setState({ loading: false }))
            .catch(_ => Alert.alert('Erro ao recuperar senha', 'Tente novamente mais tarde ou entre em contato com o administrador.'))

        } else {
            this.setState({ error: 'E-mail inválido!', email: undefined })
        }

    }

    closeMessageBox = () => {
        this.setState({ error: undefined })
    }

    render(){

        return(
            <ImageBackground style={{flex: 1}} source={require('../../assets/img/modules_background.jpg')}
                resizeMode='stretch'>
                <LinearGradient style={styles.container} 
                    colors={['rgba(255,255,255,0.2)', 'rgba(255,255,255,0.2)']}>
                    {this.state.loading ? 
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size='large' color='white'/>
                    </View> :
                    <ScrollView contentContainerStyle={{flex: 1}} bounces={false}
                        keyboardShouldPersistTaps='handled'>
                        <View style={styles.submitArea}>
                            <View style={styles.mailField}>
                                <Text style={styles.label}>Digite seu e-mail:</Text>
                                <TextInput style={styles.textInput} textContentType='emailAddress'
                                    maxLength={254} value={this.state.email} keyboardType='email-address'
                                    onChangeText={(email) => this.setState({ email })}
                                    returnKeyType='go' selectionColor='#FFF' 
                                    onSubmitEditing={this.sendMail}
                                />
                            </View>
                            <TouchableHighlight underlayColor='rgba(250,183,0, 0.5)' style={styles.button}
                                onPress={this.sendMail}>
                                <Text style={styles.buttonLabel}>ENVIAR</Text>
                            </TouchableHighlight>
                        </View>
                        {this.state.error ?
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <View style={[styles.message, { backgroundColor: 
                                    this.state.error.includes('Email enviado') ? '#4BE656' : '#EF7C7C', 
                                    borderColor: this.state.error.includes('Email enviado') ? 'green' : 
                                    'red' }]}>
                                    <Text style={[styles.messsageLabel, {
                                        color: this.state.error.includes('Email enviado') ? '#038D0C' : 
                                        '#B50606' }]}>{this.state.error}</Text>
                                    <TouchableWithoutFeedback onPress={this.closeMessageBox}>
                                        <View style={{ height: '100%', width: '10%', 
                                            alignItems: 'flex-end', justifyContent: 'flex-start' }}>
                                            <FontAwesome name='close' color='gray'
                                                size={Dimensions.get('window').width * 0.05}
                                                style={{ padding: 4 }}/>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View> : null}
                    </ScrollView> }
                </LinearGradient>
            </ImageBackground>
        )
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    submitArea: {
        height: '40%',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    mailField: {
        height: '40%', 
        width: '100%', 
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.06,
        color: '#FFF',
        width: '80%',
        textAlign: 'left',
        marginBottom: 10
    },
    textInput: {
        fontFamily: 'roboto-regular',
        color: '#E6E6E6',
        borderColor: '#FFF',
        borderBottomWidth: 1,
        width: '80%',
        paddingTop: 7,
        fontSize: Dimensions.get('window').width * 0.055
    },
    button: {
        borderRadius: 10,
        borderColor: '#0E0233',
        borderWidth: 2,
        height: '20%',
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FAB700'
    },
    buttonLabel: {
        fontFamily: 'roboto-bold',
        color: '#0E0233',
        fontSize: Dimensions.get('window').width * 0.07
    },
    message: {
        height: '20%', 
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 10
    },
    messsageLabel: {
        width: '90%',
        fontFamily: 'roboto-bold',
        fontSize: Dimensions.get('window').width * 0.055,
        textAlign: 'center',
        padding: 4
    }
})